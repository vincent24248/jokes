package com.m.jokes;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.maps.model.Dash;
import com.m.jokes.adapter.DrawerRecyclerAdapter;
import com.m.jokes.application.JokesApplication;
import com.m.jokes.constant.AppConstant;
import com.m.jokes.interfaces.OnDialogButtonClicked;
import com.m.jokes.prefrence.AppSharedPreference;
import com.m.jokes.prefrence.PrefConstants;
import com.m.jokes.ui.activity.ActivityAddVideo;
import com.m.jokes.ui.activity.ActivityFavorite;
import com.m.jokes.ui.activity.ActivityAppOfTheDay;
import com.m.jokes.ui.activity.NotesActivity;
import com.m.jokes.ui.fragment.DashBoardFragment;
import com.m.jokes.utils.Globalutils;
import com.m.jokes.utils.Utils;
import com.pollfish.interfaces.PollfishClosedListener;
import com.pollfish.interfaces.PollfishSurveyCompletedListener;
import com.pollfish.interfaces.PollfishSurveyNotAvailableListener;
import com.pollfish.interfaces.PollfishUserNotEligibleListener;
import com.pollfish.main.PollFish;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppSDK;
import static com.m.jokes.constant.AppConstant.ACTION_LOCAL_PUSH_COUNT;
import static com.m.jokes.utils.Constants.INAPP_BILLING_RODUCT_KEY;
import static com.m.jokes.utils.Constants.IN_APP_BILLING_LICENCE_KEY;
import static com.m.jokes.utils.Constants.POLLFISH;
import static com.m.jokes.utils.Constants.POSITION_RIGHT_BOTTOM;
import static com.m.jokes.utils.Constants.START_APP_ID;

public class DashBoardActivity extends AppCompatActivity implements DrawerRecyclerAdapter.OnItemCLickListner,OnDialogButtonClicked , BillingProcessor.IBillingHandler,PollfishClosedListener {
    Activity activity;
    Fragment currentFragment;
    FragmentManager fragmentManager;
    private CharSequence mTitle;
    private String[] mPlanetTitles;
    private int[] mPlanetImages;
    private DrawerLayout mDrawerLayout;
    private RecyclerView mrecycleList;
    private DrawerRecyclerAdapter drawerRecyclerAdapter;
    public Toolbar toolbar;
    BroadcastReceiver receiver;
   // private StartAppAd startAppAd ;
    BillingProcessor billingProcessor;
    InterstitialAd interstitialAd;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE,false,DashBoardActivity.this))
        {
            Utils.showPollifishAddRight(DashBoardActivity.this,POSITION_RIGHT_BOTTOM);
            StartAppSDK.init(this, START_APP_ID, false); //TODO: Replace with your Application ID
            StartAppAd.disableSplash();
            interstitialAd= Utils.intrestialAddbject(DashBoardActivity.this);

        }
        this.activity = this;
        fragmentManager = getSupportFragmentManager();
        initialiseViews();

        if (savedInstanceState == null) {
            replaceFragment(0, new DashBoardFragment(), "DF");

        }

        billingProcessor = new BillingProcessor(this, IN_APP_BILLING_LICENCE_KEY, this);

    }

    /*
     * Get Navigation drawer
     */
    public DrawerLayout getmDrawerLayout() {
        return mDrawerLayout;
    }
    protected void onStart() {
        super.onStart();
    }

    private void initialiseViews() {
        this.mTitle = "Jokes";
        toolbar = (Toolbar) findViewById(R.id.htab_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(mTitle);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        toolbar.setNavigationIcon(R.mipmap.ic_drawer);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!mDrawerLayout.isDrawerOpen(GravityCompat.START))
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });
        mPlanetTitles = getResources().getStringArray(R.array.planets_array);
        mPlanetImages = getResources().getIntArray(R.array.nav_drawer_items_icons);

        mrecycleList = (RecyclerView) findViewById(R.id.left_drawer);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mrecycleList.setLayoutManager(linearLayoutManager);
        drawerRecyclerAdapter = new DrawerRecyclerAdapter(this,mPlanetTitles,mPlanetImages,this);
        mrecycleList.setAdapter(drawerRecyclerAdapter);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//            }
//        },1000);

    }
    private void hideKeyboard() {
        View view = activity.getCurrentFocus();
        InputMethodManager imm = (InputMethodManager) activity
                .getSystemService(Activity.INPUT_METHOD_SERVICE);

        imm.hideSoftInputFromWindow(view.getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public void replaceFragment(int position, Fragment fragment, String fragmentName) {
        if (fragment != null) {
            this.fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();

        }
        this.currentFragment = fragment;
    }
    public void replaceFragmentFromDrawer(Fragment fragment, String fragmentName) {
        if (fragment != null) {

            if (fragmentManager.getBackStackEntryCount() > 0) {
                fragmentManager.popBackStack();
            }
            fragmentManager.beginTransaction().add(R.id.frame_container, fragment, fragmentName).addToBackStack("").commit();

        }
        this.currentFragment = fragment;
    }

    @Override
    public void onBackPressed() {

        if (fragmentManager == null) {
            toolbar.setVisibility(View.VISIBLE);
            return;
        }
        if (mDrawerLayout.isDrawerVisible(GravityCompat.START)) {
            toolbar.setVisibility(View.VISIBLE);
            mDrawerLayout.closeDrawer(GravityCompat.START);
            return;
        }
        if (fragmentManager.getBackStackEntryCount() > 1) {
            toolbar.setVisibility(View.GONE);
            fragmentManager.popBackStack();
            if (mDrawerLayout.isDrawerVisible(GravityCompat.START)) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
                return;
            }
            return;
        } else if (fragmentManager.getBackStackEntryCount() == 1) {
            toolbar.setVisibility(View.VISIBLE);
            fragmentManager.popBackStack();
            if (mDrawerLayout.isDrawerVisible(GravityCompat.START)) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
                return;
            }else
            {
                replaceFragment(0, new DashBoardFragment(), "DF");
            }
        }else
        {
            toolbar.setVisibility(View.VISIBLE);
            //finish();
        }
        /*if(startAppAd!=null) {
            startAppAd.showAd();
        }*/
        dialogAppExit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //  getMenuInflater().inflate(R.menu.dashboard_menu, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public void OnItemCLick(int pos){
        mDrawerLayout.closeDrawers();
        handleDraweritemClick(pos);
    }

    private void handleDraweritemClick(int pos) {
       switch (pos)
       {
           case 0:
               break;
          /* case 1:
               replaceFragment(0, new DashBoardFragment(), "DF");
               break;*/
           case 1:
               Intent mFavIntent=new Intent(DashBoardActivity.this, ActivityFavorite.class);
               mFavIntent.putExtra(AppConstant.ACTIVITY_TYPE, AppConstant.ACTIVITY_CATEGORY);
               startActivity(mFavIntent);
               break;
           case 2:
               Intent mNotesintent=new Intent(DashBoardActivity.this, NotesActivity.class);
               startActivity(mNotesintent);
               break;
           case 3:
               if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE, false, JokesApplication.getInstance()))
               {
                   billingProcessor.purchase(DashBoardActivity.this, INAPP_BILLING_RODUCT_KEY);
               }
               else
               {
                   Toast.makeText(this, "You have already purchased Adds free", Toast.LENGTH_SHORT).show();
                   replaceFragment(0, new DashBoardFragment(), "DF");
               }
               break;
           case 4:
               if(Utils.isNetworkConnected(DashBoardActivity.this))
               {
                   //if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE,false, DashBoardActivity.this))
                  // {
                       Intent mIntentAppOfTheDay = new Intent(DashBoardActivity.this, ActivityAppOfTheDay.class);
                       startActivity(mIntentAppOfTheDay);
                  // }
               }else
               {
                   Toast.makeText(DashBoardActivity.this,"No internet connection",Toast.LENGTH_SHORT).show();
               }


               break;
           case 5:
               if(Utils.isNetworkConnected(DashBoardActivity.this)) {
                  // if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE,false,DashBoardActivity.this))
                   //{
                       Intent mIntentWatchaVideo = new Intent(DashBoardActivity.this, ActivityAddVideo.class);
                       startActivity(mIntentWatchaVideo);
                   // }
                   }else
                   {
                   Toast.makeText(DashBoardActivity.this,"No internet connection",Toast.LENGTH_SHORT).show();
                   }
               break;
           case 6:
            Globalutils.ourMoreApps(DashBoardActivity.this);
               break;
           case 7:
             Globalutils.showNow(DashBoardActivity.this);
               break;
           case 8:
               Globalutils.openGoogleplayStore(DashBoardActivity.this);
               break;
           case 9:
               Globalutils.shareApp(DashBoardActivity.this);
               break;
           case 10:
               Globalutils.notificationSettingDailog(DashBoardActivity.this,this);
               break;
           case 11:
               //Globalutils.notificationSettingDailog(DashBoardActivity.this,this);
               break;
       }

    }

    @Override
    public void onDialogButtonClicked(int btnId) {

    }

    @Override
    public void onDialogButtonClicked(int btnId, String tag) {

    }
    @Override
    protected void onResume() {
        super.onResume();
        registerReciever();
        updateNotificationCount();
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_LOCAL_PUSH_COUNT);
        receiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                updateNotificationCount();
            }
        };
        registerReceiver(receiver, filter);
    }

    private void updateNotificationCount() {
        int count = AppSharedPreference.getInt(PrefConstants.RECEVED_NOTIFICATION_COUNT, 0, getApplicationContext());
        if (count > 0) {
           // txtNotificationCount.setText(" " + count + " ");
           // txtNotificationCount.setVisibility(View.VISIBLE);
        } else {
           // txtNotificationCount.setVisibility(View.GONE);
        }

    }
    @Override
    protected void onStop() {
        try {
            unregisterReceiver(receiver);
        } catch (IllegalArgumentException ex) {
            // Do nothing as receiver is already not registered
        }
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!billingProcessor.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    @Override
    public void onProductPurchased(@NonNull String s, @Nullable TransactionDetails transactionDetails)
    {
        Toast.makeText(DashBoardActivity.this,"You have purchased Successfully.",Toast.LENGTH_SHORT).show();
        AppSharedPreference.putBoolean(PrefConstants.IS_ADDS_FREE, true, JokesApplication.getInstance());

    }

    @Override
    public void onPurchaseHistoryRestored() {
       // Toast.makeText(DashBoardActivity.this,"onPurchaseHistoryRestored",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBillingError(int i, @Nullable Throwable throwable) {
        Toast.makeText(DashBoardActivity.this,"Some error occured.",Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onBillingInitialized() {
        //Toast.makeText(DashBoardActivity.this,"onBillingInitialized",Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onDestroy() {
        if (billingProcessor != null) {
            billingProcessor.release();
        }
        super.onDestroy();
    }

    public  void dialogAppExit(){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_app_exit);
        RelativeLayout btnPositive=(RelativeLayout)dialog.findViewById(R.id.positive_btn) ;
        TextView btnNegative=(TextView)dialog.findViewById(R.id.btn_negative) ;
        ImageView btnCross=(ImageView)dialog.findViewById(R.id.btn_cross);
        btnCross.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.cancel();
            }
        });

        btnPositive.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
            Globalutils.openGoogleplayStore(DashBoardActivity.this);
                dialog.dismiss();
                finish();
            }
        });
        btnNegative.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(interstitialAd!=null)
                {
                    if(interstitialAd.isLoaded())
                    {
                        interstitialAd.show();
                    }
                }
                dialog.dismiss();
                finish();
            }
        });
        dialog.show();
    }

    @Override
    public void onPollfishClosed () {
        Log.e("Pollfish", "Survey closed!");
    }

}
