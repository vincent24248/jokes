package com.m.jokes.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.m.jokes.R;
import com.m.jokes.application.JokesApplication;
import com.m.jokes.constant.AppConstant;
import com.m.jokes.model.CategoryModel;
import com.m.jokes.prefrence.AppSharedPreference;
import com.m.jokes.prefrence.PrefConstants;
import com.m.jokes.ui.activity.ActivityAddVideo;
import com.m.jokes.ui.activity.ActivityAppOfTheDay;
import com.m.jokes.ui.activity.ActivityFavorite;
import com.m.jokes.ui.activity.ActivityIntrestialAdds;
import com.m.jokes.ui.activity.NotesActivity;
import com.m.jokes.ui.activity.SubCategoryActivity;
import com.m.jokes.utils.Globalutils;

import java.util.ArrayList;

/**
 * Created by SUJEET on 25-11-2017.
 * 
 */

public class CategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private ArrayList<CategoryModel> categoryList;
    private Activity context;
    public CategoryAdapter(Activity context, ArrayList<CategoryModel> categoryList)
    {
        this.context = context;
        this.categoryList = categoryList;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView ;
        int TYPE_HEADER = 0;
        int TYPE_CATEGORY_DATA=1;
        if (viewType == TYPE_HEADER)
        {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_header_item, parent, false);
            return new CategoryAdapter.DashboardViewHolder(itemView);
        }else if(viewType==TYPE_CATEGORY_DATA)
        {
            itemView = LayoutInflater.from(context).inflate(R.layout.recyclerview_category, parent, false);
            return new CategoryAdapter.CategoryViewHolder(itemView);

        }else
        {
            return null;
        }

    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, final int positn)
    {

    }


    class CategoryViewHolder extends RecyclerView.ViewHolder
    {
        CategoryViewHolder(View itemView)
        {
            super(itemView);
//            recyclerView = (RecyclerView) itemView.findViewById(R.id.cate_list);
//            GridLayoutManager gridLayoutManager = new GridLayoutManager(context,3);
//            CategorAdapter = new DashboardCategoryAdapter(context, categoryList);
//            recyclerView.setLayoutManager(gridLayoutManager);
//            recyclerView.scrollToPosition(3);
//            recyclerView.setAdapter(CategorAdapter);
//            recyclerView.setOnTouchListener(new View.OnTouchListener() {
//                @Override
//                public boolean onTouch(View view, MotionEvent motionEvent) {
//                    view.getParent().requestDisallowInterceptTouchEvent(true);
//                    return false;
//                }
//            });
        }
    }



    public class DashboardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {

        TextView txtCategoryNameOne;
        RelativeLayout relayOne;
        TextView txtCategoryNameTwo;
        RelativeLayout relayTwo;
        RelativeLayout  mAdView ;

        DashboardViewHolder(View itemView)
        {
            super(itemView);
            txtCategoryNameOne=(TextView)itemView.findViewById(R.id.tv_category);
            relayOne=(RelativeLayout) itemView.findViewById(R.id.rel1);
            relayOne.setOnClickListener(this);

            txtCategoryNameTwo=(TextView)itemView.findViewById(R.id.tv_category1);
            relayTwo=(RelativeLayout)itemView.findViewById(R.id.rel2);
            relayTwo.setOnClickListener(this);

            mAdView= (RelativeLayout)itemView.findViewById(R.id.adView);

         if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE, false, JokesApplication.getInstance()))
           {
            mAdView.setVisibility(View.VISIBLE);
          }else
         {
             mAdView.setVisibility(View.GONE);
         }
             itemView.findViewById(R.id.rel3).setOnClickListener(this);
             itemView.findViewById(R.id.rel4).setOnClickListener(this);
             itemView.findViewById(R.id.rel5).setOnClickListener(this);
             itemView.findViewById(R.id.rel6).setOnClickListener(this);


            txtCategoryNameOne.setText(categoryList.get(0).getCategory());
            relayOne.setBackgroundColor(Globalutils.getRandonColor());

            txtCategoryNameTwo.setText(categoryList.get(1).getCategory());
            relayTwo.setBackgroundColor(Globalutils.getRandonColor());
            //mAdView.loadAd(Utils.getAdRefence(context));

        }



        @Override
        public void onClick(View view) {
            switch (view.getId())
            {
                case R.id.rel1:
                    Intent intent = new Intent(context, SubCategoryActivity.class);
                    intent.putExtra("category", categoryList.get(0).getCategory());
                    intent.putExtra(AppConstant.ACTIVITY_TYPE, AppConstant.ACTIVITY_CATEGORY);
                    context.startActivity(intent);
                    break;
                case R.id.rel2:
                    Intent intentt = new Intent(context, SubCategoryActivity.class);
                    intentt.putExtra("category", categoryList.get(1).getCategory());
                    intentt.putExtra(AppConstant.ACTIVITY_TYPE, AppConstant.ACTIVITY_CATEGORY);
                    context.startActivity(intentt);
                    break;
                case R.id.rel3:
                    context.startActivity(new Intent(context, NotesActivity.class));
                    break;
                case R.id.rel4:
                   // if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE,false,context))
                   // {
                        context.startActivity(new Intent(context, ActivityAppOfTheDay.class));
                    //}
                    break;
                case R.id.rel5:
                    context.startActivity(new Intent(context, ActivityFavorite.class));
                    break;
                case R.id.rel6:
                    //if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE,false,context))
                    //{
                        Intent mIntent = new Intent(context, ActivityAddVideo.class);
                        context.startActivity(mIntent);
                   // }
                    break;

            }
        }
    }


    @Override
    public int getItemViewType(int position)
    {
        return position;
    }


    @Override
    public int getItemCount()
    {
        return 2;
    }
}
