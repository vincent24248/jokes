package com.m.jokes.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.m.jokes.R;
import com.m.jokes.constant.AppConstant;
import com.m.jokes.model.CategoryModel;
import com.m.jokes.ui.activity.SubCategoryActivity;
import com.m.jokes.utils.Globalutils;
import java.util.ArrayList;

/**
 * Created by SUJEET on 25-11-2017.
 * 
 */

public class DashboardCategoryAdapter extends RecyclerView.Adapter<DashboardCategoryAdapter.CategoryViewHolder> {
    private ArrayList<CategoryModel> categoryList;

    private Activity context;
    public DashboardCategoryAdapter(Activity context, ArrayList<CategoryModel> categoryList)
    {
        this.context = context;
        categoryList.remove(0);
        categoryList.remove(0);
        this.categoryList = categoryList;
    }



    @Override
    public DashboardCategoryAdapter.CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView ;
        itemView =LayoutInflater.from(context).inflate(R.layout.recyclerview_category, parent, false);
        return new DashboardCategoryAdapter.CategoryViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(DashboardCategoryAdapter.CategoryViewHolder viewholder, final int positn) {
        final int position = viewholder.getAdapterPosition();
        viewholder.txtCategoryLetter.setText(categoryList.get(position).getCategory().substring(0,1).toUpperCase());
        viewholder.txtCateoryName.setText(categoryList.get(position).getCategory());
        viewholder.relayParent.setBackgroundColor(Globalutils.getRandonColor());
        viewholder.relayParent.setOnClickListener(new View.OnClickListener()
        {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent(context, SubCategoryActivity.class);
                    intent.putExtra("category", categoryList.get(position).getCategory());
                    intent.putExtra(AppConstant.ACTIVITY_TYPE, AppConstant.ACTIVITY_CATEGORY);
                    context.startActivity(intent);
                }
            });

    }


    class CategoryViewHolder extends RecyclerView.ViewHolder {
       // private final TextView tv_identifier;
        private final TextView txtCategoryLetter,txtCateoryName;
        RelativeLayout relayParent;

        CategoryViewHolder(View itemView) {
            super(itemView);
            txtCategoryLetter = (TextView) itemView.findViewById(R.id.txt_category_letter);
            txtCateoryName = (TextView) itemView.findViewById(R.id.txt_category_name);
            relayParent = (RelativeLayout) itemView.findViewById(R.id.relay_parent);

        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public int getItemCount() {
        return categoryList.size();
    }
}
