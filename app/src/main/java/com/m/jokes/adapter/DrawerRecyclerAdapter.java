package com.m.jokes.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.m.jokes.R;
import com.m.jokes.utils.Globalutils;
import static com.m.jokes.R.id.btn_fb;
import static com.m.jokes.R.id.btn_google;
import static com.m.jokes.R.id.btn_utube;

/**
 * Created by lab1 on 24/07/17.
 *
 */

public class DrawerRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context context ;
    private String [] mPlanetTitles;
    private  OnItemCLickListner onItemCLickListner ;
    int[] mPlanetImages;
    public DrawerRecyclerAdapter(Context context, String[] mPlanetTitles, int[] mPlanetImages , OnItemCLickListner onItemCLickListner)
    {
        this.context = context ;
        this.mPlanetTitles = mPlanetTitles ;
        this.mPlanetImages = mPlanetImages ;
        this.onItemCLickListner = onItemCLickListner;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView ;
        int TYPE_HEADER = 0;
        int TYPE_FOOTER=11;
        if (viewType == TYPE_HEADER) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_header_drawer, parent, false);
            return new DrawerRecyclerAdapter.LIHeader(itemView);
        }else if(viewType==TYPE_FOOTER)
        {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_drawer_footer, parent, false);
            return new DrawerRecyclerAdapter.LIFooter(itemView);
        }
            else
         {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.residemenu_item, parent, false);
            return new DrawerRecyclerAdapter.DrawerViewHolder(itemView);

        }


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof LIHeader) {
            LIHeader drawerViewHolder = (LIHeader) holder ;
            drawerViewHolder.name.setText("User Name");
            drawerViewHolder.email.setText("user.name@jokes.com");


        } else if (holder instanceof LIFooter) {



        }else {
            TypedArray imgs ;
            imgs = context.getResources().obtainTypedArray(R.array.nav_drawer_items_icons);
            DrawerViewHolder drawerViewHolder = (DrawerViewHolder)holder ;
            drawerViewHolder.imagView.setImageResource(imgs.getResourceId(position, -1));
            drawerViewHolder.itemName.setText(mPlanetTitles[position]);

        }
    }

    @Override
    public int getItemCount() {
        return mPlanetTitles.length;
    }

    public class DrawerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView itemName;
        private final RelativeLayout itemParentView;
        private  ImageView imagView;

        DrawerViewHolder(View itemView) {
            super(itemView);
            itemName = (TextView) itemView.findViewById(R.id.tv_title);
            itemParentView = (RelativeLayout) itemView.findViewById(R.id.drawerItemRelative);
            imagView = (ImageView) itemView.findViewById(R.id.iv_icon);
            itemParentView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            onItemCLickListner.OnItemCLick(getAdapterPosition());
        }
    }


    public class LIHeader extends RecyclerView.ViewHolder implements View.OnClickListener{
        private final ImageView profileImg;
        private final TextView name;
        private final TextView email;
        RelativeLayout mRelProfile;

        LIHeader(View itemView) {
            super(itemView);
            mRelProfile = (RelativeLayout) itemView.findViewById(R.id.rel_profile);
            profileImg = (ImageView) itemView.findViewById(R.id.profile_img);
            name = (TextView) itemView.findViewById(R.id.name);
            email = (TextView) itemView.findViewById(R.id.email);
            mRelProfile.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
        }
    }
    public class LIFooter extends RecyclerView.ViewHolder implements View.OnClickListener{


        LIFooter(View itemView) {
            super(itemView);
           itemView.findViewById(btn_fb).setOnClickListener(this);
            itemView.findViewById(btn_google).setOnClickListener(this);
            itemView.findViewById(btn_utube).setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId())
            {
                case btn_fb:
              Globalutils.openFacebookLink(context);
                    break;
                case btn_google:
                    Globalutils.openGoogleLink(context);
                    break;
                case btn_utube:
                    Globalutils.openUtubeLink(context);
                    break;

            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public interface OnItemCLickListner{
        void OnItemCLick(int pos);
    }
}
