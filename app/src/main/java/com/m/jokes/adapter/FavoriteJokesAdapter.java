package com.m.jokes.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.m.jokes.R;
import com.m.jokes.application.JokesApplication;
import com.m.jokes.model.JokesModel;
import com.m.jokes.prefrence.AppSharedPreference;
import com.m.jokes.prefrence.PrefConstants;
import com.m.jokes.ui.activity.JokeDetailsActivity;
import com.m.jokes.utils.Globalutils;
import com.startapp.android.publish.ads.nativead.NativeAdDetails;
import java.util.ArrayList;
import static com.m.jokes.constant.AppConstant.SELECTED_POSITION;
import static com.m.jokes.utils.Constants.FROM_FAVOURITE;
import static com.m.jokes.utils.Constants.IS_COMING_FROM;

/**
 * Created by SUJEET on 25-11-2017.
 * 
 */

public class FavoriteJokesAdapter extends RecyclerView.Adapter<FavoriteJokesAdapter.CategoryViewHolder> {

    private ArrayList<JokesModel> categoryList;
    ArrayList<NativeAdDetails> addsModels;
    private Activity context;
    public FavoriteJokesAdapter(Activity context, ArrayList<JokesModel> categoryList) {
        this.context = context;
        this.categoryList = categoryList;
    }



    @Override
    public FavoriteJokesAdapter.CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(context).inflate(R.layout.recyclerview_fav_item, parent, false);
        return new FavoriteJokesAdapter.CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FavoriteJokesAdapter.CategoryViewHolder holder, final int positn) {
        final int position = holder.getAdapterPosition();
        if(categoryList.get(position)!=null&&categoryList.get(position).getNativeAdDetails()!=null&&addsModels!=null&&addsModels.size()>0)
        {
            holder.relViewAdd.setVisibility(View.VISIBLE);
            holder.relayParent.setVisibility(View.GONE);
            holder.titleAdds.setText(categoryList.get(position).getNativeAdDetails().getTitle());
            holder.descriptionAdds.setText(categoryList.get(position).getNativeAdDetails().getDescription());
            holder.imgAdds.setImageBitmap(categoryList.get(position).getNativeAdDetails().getImageBitmap());
            //holder.mAddview.showBanner();
           // holder.relayParent.setVisibility(View.GONE);
            //holder.mAddview.loadAd(Utils.getAdRefence(context));
           // holder.mAddview.bringToFront();
        }else {
           // holder.mAddview.hideBanner();
            holder.relViewAdd.setVisibility(View.GONE);
            holder.relayParent.setVisibility(View.VISIBLE);
            String name = categoryList.get(position).getSubCategory();
            String[] splitName = name.split(" ");
            if (splitName.length > 1)
                holder.tv_identifier.setText(splitName[0].charAt(0) + "" + splitName[1].charAt(0));
            else
                holder.tv_identifier.setText(splitName[0].charAt(0));
            Drawable background = holder.tv_identifier.getBackground();
            ((GradientDrawable)background).setColor(Globalutils.getRandonColor());
            holder.tv_category.setText(categoryList.get(position).getSubCategory());
            holder.tv_msg.setText(categoryList.get(position).getMessage());
        }
        holder.notesCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (categoryList.get(position).getNativeAdDetails()==null)
                {
                       Intent intent = new Intent(context, JokeDetailsActivity.class);
                       intent.putExtra(SELECTED_POSITION,categoryList.get(position).getPosition());
                       intent.putExtra(IS_COMING_FROM,FROM_FAVOURITE);
                       context.startActivity(intent);
                }else
                {
                    categoryList.get(position).getNativeAdDetails().sendClick(context);
                }

               }
        });
    }


    class CategoryViewHolder extends RecyclerView.ViewHolder {
        private final TextView tv_identifier;
        private final TextView tv_msg;
        private final TextView tv_category;
        CardView notesCardView;
       // com.startapp.android.publish.ads.banner.Banner mAddview;
        RelativeLayout relayParent;

        //Adds view
        RelativeLayout relViewAdd;
        ImageView imgAdds;
        TextView titleAdds,descriptionAdds;

        CategoryViewHolder(View itemView)
        {
            super(itemView);
            tv_identifier = (TextView) itemView.findViewById(R.id.nameTxt);
            tv_category = (TextView) itemView.findViewById(R.id.tv_category);
            tv_msg = (TextView) itemView.findViewById(R.id.tv_msg);
            notesCardView = (CardView) itemView.findViewById(R.id.notesCardView);
            //mAddview=(com.startapp.android.publish.ads.banner.Banner)itemView.findViewById(R.id.adView);
            relayParent=(RelativeLayout)itemView.findViewById(R.id.rel_view);

            //Adds view
            relViewAdd=(RelativeLayout)itemView.findViewById(R.id.rel_view_add);
            imgAdds=(ImageView)itemView.findViewById(R.id.img_add);
            titleAdds=(TextView)itemView.findViewById(R.id.text_title);
            descriptionAdds=(TextView)itemView.findViewById(R.id.txt_description);
        }
    }

    /*public void setAdapterData(ArrayList<JokesModel> categoryList, ArrayList<NativeAdDetails> addsModels)
    {
        this.categoryList=categoryList;
        this.addsModels=addsModels;
        notifyDataSetChanged();
    }*/

    public void setAdapterData(ArrayList<JokesModel> catList, ArrayList<NativeAdDetails> addsModels)
    {
        categoryList.clear();
        this.addsModels=addsModels;

        // if(addsModels!=null)
        // Toast.makeText(context,""+addsModels.size(),Toast.LENGTH_SHORT).show();
        if(catList!=null&&catList.size()>0&&addsModels!=null&&addsModels.size()>0)
        {
            for(int i=0;i<catList.size();i++)
            {
                if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE, false, JokesApplication.getInstance()))
                {
                    if (i % 3 == 0)
                    {
                        JokesModel jokesModel=new JokesModel();
                        if((addsModels.size()<=i))
                            jokesModel.setNativeAdDetails(addsModels.get(addsModels.size()-1));
                        else
                            jokesModel.setNativeAdDetails(addsModels.get(i));
                        categoryList.add(jokesModel);

                    }
                }
                categoryList.add(catList.get(i));

            }

        }else
        {
            categoryList.addAll(catList);
        }
        Log.e("catSize ",""+categoryList.size());
        notifyDataSetChanged();
    }







    @Override
    public int getItemCount()
    {
        return categoryList.size();
    }
}
