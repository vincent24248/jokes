package com.m.jokes.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.m.jokes.R;
import com.m.jokes.application.JokesApplication;
import com.m.jokes.model.JokesModel;
import com.m.jokes.prefrence.AppSharedPreference;
import com.m.jokes.prefrence.PrefConstants;
import com.m.jokes.utils.Globalutils;
import com.startapp.android.publish.ads.nativead.NativeAdDetails;
import java.util.ArrayList;
import static com.m.jokes.utils.Constants.DEFAULT_TEXT_SIZE;

/**
 * Created by SUJEET on 07-12-2017.
 */

public class JokeDetailsAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    private Context mContext ;
    private ArrayList<JokesModel> layouts  ;
    public View view ,mCurrentView;
    NativeAdDetails nativeAdDetails;

    public JokeDetailsAdapter(Context mContext,ArrayList<JokesModel> layouts) {
        this.mContext = mContext ;
        this.layouts = layouts ;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.item_joke_details, container, false);
        TextView tvTitle = (TextView) view.findViewById(R.id.tv_title);
        TextView tvTotalJokes = (TextView) view.findViewById(R.id.tv_total_item);
        TextView tvDetails = (TextView) view.findViewById(R.id.tv_details);
        tvTitle.setText(layouts.get(position).getSubCategory());
        tvDetails.setText(layouts.get(position).getMessage());
        ScrollView mainlayout=(ScrollView)view.findViewById(R.id.mainlayout);
        mainlayout.setBackgroundColor(Globalutils.getRandonColor());
        int count=position+1;
        tvTotalJokes.setText(""+count+"/"+layouts.size());
        tvDetails.setTextSize(AppSharedPreference.getInt(PrefConstants.TEXT_SIZE, DEFAULT_TEXT_SIZE, JokesApplication.getInstance()));
        view.setTag("view" + position);

        RelativeLayout addsRelay=(RelativeLayout)view.findViewById(R.id.adds_image_relay);
        TextView txtTitle=(TextView)view.findViewById(R.id.text_title);
        RelativeLayout btnInstall=(RelativeLayout)view.findViewById(R.id.btn_install);
        ImageView addSmallImg=(ImageView)view.findViewById(R.id.add_img_small);
        RatingBar ratingBar=(RatingBar)view.findViewById(R.id.rating_bar);
        TextView txtDescription=(TextView)view.findViewById(R.id.txt_description);
        ImageView addsImg=(ImageView)view.findViewById(R.id.add_img_startapp);
        if(nativeAdDetails!=null&&nativeAdDetails.getImageBitmap()!=null)
        {
            addsRelay.setVisibility(View.VISIBLE);
            txtTitle.setText(nativeAdDetails.getTitle());
            addsImg.setImageBitmap(nativeAdDetails.getImageBitmap());
            txtDescription.setText(nativeAdDetails.getDescription());
            addSmallImg.setImageBitmap(nativeAdDetails.getSecondaryImageBitmap());
            ratingBar.setRating(nativeAdDetails.getRating());
            if(nativeAdDetails.isApp())
            {
                btnInstall.setVisibility(View.VISIBLE);
            }else
            {
                btnInstall.setVisibility(View.GONE);
            }

        }else
        {
            addsRelay.setVisibility(View.GONE);
        }
        addsRelay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nativeAdDetails.sendClick(mContext);
            }
        });
        btnInstall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nativeAdDetails.sendClick(mContext);
            }
        });
        container.addView(view);

        return view;
    }

    public View getAdapterView()
    {
        return view ;
    }
    @Override
    public int getCount() {
        return layouts.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        mCurrentView = (View)object;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
    public void setAddsData(NativeAdDetails nativeAdDetails)
    {
        this.nativeAdDetails=nativeAdDetails;
    }
}
