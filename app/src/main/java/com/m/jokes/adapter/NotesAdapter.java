package com.m.jokes.adapter;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.daimajia.swipe.SwipeLayout;
import com.m.jokes.R;
import com.m.jokes.db.NotesTable;
import com.m.jokes.interfaces.NoteClickCallback;
import com.m.jokes.model.NotesModel;
import com.m.jokes.utils.Utils;
import java.util.ArrayList;


/**
 * Created by Mohit.Sati on 08-06-2017.
 *
 */

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.NotesViewHolder> {

    private ArrayList<NotesModel> notesModels;
    private Activity context;
    private NotesTable mNotesTable;
    private NoteClickCallback noteClickCallback;

    public NotesAdapter(Activity context, ArrayList<NotesModel> notesModelList,NoteClickCallback noteClickCallback) {
        this.context = context;
        this.noteClickCallback = noteClickCallback;
        this.notesModels = notesModelList;
    }

    public void updateNotesModels(ArrayList<NotesModel> notesModels) {
        this.notesModels = notesModels;
    }

    @Override
    public NotesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        mNotesTable = new NotesTable(context.getApplication());
        view = LayoutInflater.from(context).inflate(R.layout.recyclerview_notes, parent, false);
        return new NotesViewHolder(view);
    }
    @Override
    public void onBindViewHolder(NotesViewHolder holder, final int positn) {
        final int position = holder.getAdapterPosition();
        holder.notes_text.setText(notesModels.get(position).getNote_text());
        if(notesModels.get(position).getNote_text().length()>9) {
            holder.notes_title.setText(notesModels.get(position).getNote_text().substring(0, 8) + "...");
        } else
            holder.notes_title.setText(notesModels.get(position).getNote_text());
//        holder.notes_text.setTypeface(AppUtils.setMediumFont(context));
        String when_updated = Utils.timestring(notesModels.get(position).getNote_date());
        if (when_updated.contains("seconds")) {
            when_updated = when_updated.replace(when_updated, "just now");
        }
        holder.delete_searched_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNotesTable.deleteNote(notesModels.get(position).getNote_date());
                notesModels = mNotesTable.fetchNote();
                notifyDataSetChanged();
            }
        });

        holder.notes_date.setText(when_updated);
//        holder.notes_date.setTypeface(AppUtils.setMediumFont(context));
        holder.notesCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noteClickCallback.openNote(notesModels.get(position).getNote_text(), notesModels.get(position).getNote_date(),position);
            }
        });
    }


    class NotesViewHolder extends RecyclerView.ViewHolder {
        private final TextView notes_title;
        private ImageView delete_searched_item;
        private CardView notesCardView;
        TextView notes_text;
        TextView notes_date;
        SwipeLayout swipe_layout;

        NotesViewHolder(View itemView) {
            super(itemView);
            swipe_layout = (SwipeLayout) itemView.findViewById(R.id.swipe_delete);
            delete_searched_item = (ImageView) itemView.findViewById(R.id.delete_searched_item);
            notes_text = (TextView) itemView.findViewById(R.id.tv_note);
            notes_title = (TextView) itemView.findViewById(R.id.title_note);
            notesCardView = (CardView) itemView.findViewById(R.id.notesCardView);
//            notes_text.setTypeface(Utils.setRegularFont(context));
            notes_date = (TextView) itemView.findViewById(R.id.tv_date_time);
//            notes_date.setTypeface(Utils.setRegularFont(context));

        }
    }

    @Override
    public int getItemCount() {
        return notesModels.size();
    }
}
