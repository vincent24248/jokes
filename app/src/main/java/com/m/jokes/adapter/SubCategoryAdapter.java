package com.m.jokes.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.m.jokes.R;
import com.m.jokes.application.JokesApplication;
import com.m.jokes.model.AddsModel;
import com.m.jokes.model.JokesModel;
import com.m.jokes.prefrence.AppSharedPreference;
import com.m.jokes.prefrence.PrefConstants;
import com.m.jokes.ui.activity.JokeDetailsActivity;
import com.m.jokes.utils.Globalutils;
import com.startapp.android.publish.ads.nativead.NativeAdDetails;
import java.util.ArrayList;
import static com.m.jokes.constant.AppConstant.SELECTED_POSITION;
import static com.m.jokes.constant.AppConstant.SUBCATEGORY_NAME;
import static com.m.jokes.utils.Constants.FROM_SUBCATEGORY;
import static com.m.jokes.utils.Constants.IS_COMING_FROM;

/**
 * Created by SUJEET on 25-11-2017.
 * 
 */

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.CategoryViewHolder> {
    private ArrayList<JokesModel> categoryList;
    private ArrayList<NativeAdDetails> addsModels;
    private Activity context;
    private String mCategory;

    public SubCategoryAdapter(Activity context, ArrayList<JokesModel> categoryList, String mCategory) {
        this.context = context;
        this.categoryList = categoryList;
        this.mCategory = mCategory;
    }

    @Override
    public SubCategoryAdapter.CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view;
        view = LayoutInflater.from(context).inflate(R.layout.recyclerview_sub_category, parent, false);
        return new SubCategoryAdapter.CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SubCategoryAdapter.CategoryViewHolder holder, final int positn) {
        final int position = holder.getAdapterPosition();
        Log.e("addsPosition___ ",""+(position));
        if ( categoryList.get(position).getNativeAdDetails()!=null&&addsModels!=null&&addsModels.size()>0)
        {
            Log.e("addsPosition ",""+(position));
            holder.relViewAdd.setVisibility(View.VISIBLE);
            holder.relView.setVisibility(View.GONE);
            holder.titleAdds.setText(categoryList.get(position).getNativeAdDetails().getTitle());
            holder.descriptionAdds.setText(categoryList.get(position).getNativeAdDetails().getDescription());
            holder.imgAdds.setImageBitmap(categoryList.get(position).getNativeAdDetails().getImageBitmap());
           // holder.mAdView.showBanner();
            //holder.relView.setVisibility(View.GONE);
           // holder.mAdView.loadAd(Utils.getAdRefence(context));
           // holder.mAdView.bringToFront();
        }else {
            //holder.mAdView.hideBanner();
            holder.relViewAdd.setVisibility(View.GONE);
            holder.relView.setVisibility(View.VISIBLE);
            holder.relView.bringToFront();
            String[] splitName = mCategory.split(" ");
            if (splitName.length > 1) {
                String fullName = splitName[0].charAt(0) + "" + splitName[1].charAt(0);
                holder.tv_identifier.setText(fullName);
            }

            Drawable background = holder.tv_identifier.getBackground();
            ((GradientDrawable)background).setColor(Globalutils.getRandonColor());
            holder.tv_category.setText(categoryList.get(position).getMessage());
            holder.tv_category_header.setText(categoryList.get(position).getSubCategory());
        }
            holder.notesCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (categoryList.get(position).getNativeAdDetails()==null)
                    {
                        Intent intent = new Intent(context, JokeDetailsActivity.class);
                        intent.putExtra(SUBCATEGORY_NAME, categoryList.get(position).getCategory());
                        intent.putExtra(SELECTED_POSITION,categoryList.get(position).getPosition());
                        intent.putExtra(IS_COMING_FROM, FROM_SUBCATEGORY);
                        context.startActivity(intent);
                    }else
                    {
                        categoryList.get(position).getNativeAdDetails().sendClick(context);
                    }
                }
            });

    }

    public void setData(ArrayList<JokesModel>list, ArrayList<NativeAdDetails> addsModels)
    {
        categoryList.clear();
        this.addsModels=addsModels;
        Log.e("listSizeCataddsModels ",""+addsModels.size());
        Log.e("listSizeCat ",""+list.size());

       // if(addsModels!=null)
           // Toast.makeText(context,""+addsModels.size(),Toast.LENGTH_SHORT).show();
      if(list!=null&&list.size()>0&&addsModels!=null&&addsModels.size()>0)
      {
            for(int i=0;i<list.size();i++)
            {
                        if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE, false, JokesApplication.getInstance()))
                        {
                            if (i % 3 == 0)
                            {
                                JokesModel jokesModel=new JokesModel();
                                if((addsModels.size()<=i))
                                    jokesModel.setNativeAdDetails(addsModels.get(addsModels.size()-1));
                                else
                                    jokesModel.setNativeAdDetails(addsModels.get(i));
                                categoryList.add(jokesModel);
                            }
                        }
                        categoryList.add(list.get(i));

                }

        }else
      {
          categoryList.addAll(list);
      }
        Log.e("catSize ",""+categoryList.size());
        notifyDataSetChanged();
    }


    class CategoryViewHolder extends RecyclerView.ViewHolder
    {
        private final TextView tv_identifier;
        private final TextView tv_category;
        private final TextView tv_category_header;
        CardView notesCardView;
        RelativeLayout relView ;
       // com.startapp.android.publish.ads.banner.Banner  mAdView ;

        //Adds view
        RelativeLayout relViewAdd;
        ImageView imgAdds;
        TextView titleAdds,descriptionAdds;
        CategoryViewHolder(View itemView)
        {
            super(itemView);
            tv_identifier = (TextView) itemView.findViewById(R.id.nameTxt);
            tv_category = (TextView) itemView.findViewById(R.id.tv_category);
            tv_category_header = (TextView) itemView.findViewById(R.id.tv_category_header);
           // mAdView = (com.startapp.android.publish.ads.banner.Banner) itemView.findViewById(R.id.adView);
            relView = (RelativeLayout) itemView.findViewById(R.id.rel_view);
            notesCardView = (CardView) itemView.findViewById(R.id.notesCardView);

            //Adds view
            relViewAdd=(RelativeLayout)itemView.findViewById(R.id.rel_view_add);
            imgAdds=(ImageView)itemView.findViewById(R.id.img_add);
            titleAdds=(TextView)itemView.findViewById(R.id.text_title);
            descriptionAdds=(TextView)itemView.findViewById(R.id.txt_description);
        }
    }

    @Override
    public int getItemCount()
    {
        return categoryList!=null ?categoryList.size():0;
    }
}
