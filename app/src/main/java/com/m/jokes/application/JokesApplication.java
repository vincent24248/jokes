package com.m.jokes.application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.speech.tts.TextToSpeech;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;
import android.view.View;

import com.appnext.base.Appnext;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;
import com.m.jokes.DashBoardActivity;
import com.m.jokes.db.DataBaseHelper;
import com.m.jokes.utils.Utils;

import java.util.Locale;

import static com.m.jokes.utils.Constants.POSITION_RIGHT_BOTTOM;

/**
 * Created by SUJEET on 20-11-2017.
 *
 */

public class JokesApplication extends MultiDexApplication {
    private DataBaseHelper dataHelper;
    static JokesApplication jokesApplication;
    static  TextToSpeech textToSpeech;
    @Override
    public void onCreate() {
//        MultiDex.install(this);
        super.onCreate();
        dataHelper = getDbHelper();
        setInstance(this);
        initialiseTextToSpeech();

        //Utils.showPollifishAddRight(getInstance(),POSITION_RIGHT_BOTTOM);
    }
    public static JokesApplication getInstance() {
        return jokesApplication;
    }

    protected static void setInstance(JokesApplication mInstance) {
        JokesApplication.jokesApplication = mInstance;
    }


    public DataBaseHelper getDbHelper() {
        String dbName = "mJokes.db";
        int dbVersion = 1;
        if (dataHelper == null) {
                Log.e("App", "dataHelper ----- ---------------null ");
                try {
                    dataHelper = new DataBaseHelper(this, dbName, null, dbVersion);
                    SQLiteDatabase database = dataHelper.openDataBase();
//                  dataHelper.createDataBase();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return dataHelper;
    }
    public DataBaseHelper getDataHelper() {
        return this.dataHelper;
    }

    public void initialiseTextToSpeech()
    {
        textToSpeech=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener()
        {
            @Override
            public void onInit(int status)
            {
                if(status != TextToSpeech.ERROR)
                {
                    textToSpeech.setLanguage(Locale.UK);
                }
            }
        });
    }
    public static TextToSpeech getTextToSpeechObj()
    {
        return textToSpeech;
    }




}

