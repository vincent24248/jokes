package com.m.jokes.constant;

/**
 * Created by SUJEET on 30-11-2017.
 */

public class AppConstant {

    public static final String ACTIVITY_CATEGORY = "activity_category";
    public static final String ACTIVITY_FAVORITE = "activity_favorite";
    public static final String ACTIVITY_TYPE = "activity_type";
    public static String ACTION_LOCAL_PUSH_COUNT="com.m.jokes.action.updateNotificationCount";
    public static String SELECTED_POSITION="SELECTED_POSITION";
    public static String SUBCATEGORY_NAME="SUBCATEGORY_NAME";
    public static String CATEGORY="CATEGORY";
}
