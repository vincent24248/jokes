package com.m.jokes.db;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DataBaseHelper extends SQLiteOpenHelper {
    private final String ASSETS_DB_NAME = "mJokes.db";
    private final Context mContext;
    private final int dbVersion;
    private SQLiteDatabase database;

    public DataBaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        Log.e("sujeet", "on DataBaseHelper");
        this.dbVersion = version;
        mContext = context;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.e("App", "onCreate ----- --------------- ");
//        this.getReadableDatabase();
//        try {
        Log.i("sujeet", "on Create");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e("App", "onUpgrade ----- --------------- ");
       /* ArrayList<String> upgradeQueries = SqliteConfig.getUpgradeDbQueries(oldVersion, newVersion);
        int poistion = 0;
        if (upgradeQueries != null && upgradeQueries.size() > 0) {
            for (String table : upgradeQueries) {
              //  Log.e("Table position " + poistion + "--- " ,table);
                db.execSQL(table);
                poistion++;
            }
        }*/
    }

    private boolean createDataBase() throws IOException {
        Log.d("TAG", "createDataBase");
        boolean dbExist = checkDataBase();
        if (dbExist) {
            // do nothing - database already exist
//            Log.i("version","vrer=="+getReadableDatabase().getVersion());
        } else {
            // By calling this method and empty database will be created into
            // the default system path
            // of your application so we are gonna be able to overwrite that
            // database with our database.
            this.getReadableDatabase();
            try {
                copyDataBase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
        this.getWritableDatabase();

        return dbExist;
    }

    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;
        try {
            String myPath = mContext.getDatabasePath(this.getDatabaseName()).getPath();
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
        } catch (SQLiteException e) {
            // database does't exist yet.
        }
        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null ? true : false;
    }


    private void copyDataBase() throws IOException {
        // Open your local db as the input stream
        OutputStream myOutput = null;
        InputStream myInput = null;
        try {
            this.getReadableDatabase();
            myInput = mContext.getAssets().open(ASSETS_DB_NAME);
            Log.i("sujeet", "on COPY");

            // Path to the just created empty db
            String outFileName = mContext.getDatabasePath(this.getDatabaseName()).getPath();
            // Open the empty db as the output stream
            myOutput = new FileOutputStream(outFileName);
            // transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }
            myOutput.flush();
        } finally {
//            this.getWritableDatabase();

            if (myOutput != null)
                myOutput.close();
            if (myInput != null)
                myInput.close();
        }
        // Close the streams
    }

    public SQLiteDatabase openDataBase() {
        try {
            if (database == null) {
                boolean exist = createDataBase();
                database = SQLiteDatabase.openDatabase(mContext.getDatabasePath(this.getDatabaseName()).getPath(), null,
                        SQLiteDatabase.OPEN_READWRITE);
                if (!exist) {
                    onUpgrade(database, 0, dbVersion);
                }
//               this.getWritableDatabase();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        database.execSQL("PRAGMA user_version = " + dbVersion);
        this.getWritableDatabase();
        return database;
    }
}

