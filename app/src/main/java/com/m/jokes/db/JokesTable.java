package com.m.jokes.db;

import android.app.Application;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.m.jokes.application.JokesApplication;
import com.m.jokes.model.CategoryModel;
import com.m.jokes.model.JokesModel;
import com.m.jokes.prefrence.AppSharedPreference;
import com.m.jokes.prefrence.PrefConstants;

import java.util.ArrayList;

/**
 * Created by SUJEET on 22-11-2017.
 *
 */

public class JokesTable {
    private static final String JOKES_TABLE = "jokes";
//    private static final String CATEGORY = "Category";
//    private static final String SUB_CATEGORY = "SubCategory";
//    private static final String MESSAGE = "Message";
    private static final String IS_FAVOURITED = "isFavourited";

    private static final String SELECT_CATEGORY_QUERY = "select distinct Category from jokes ";
//    private static final String SELECT_CATEGORY_QUERY = "select Category from jokes ";
    private static final String SELECT_SUBCATEGORY_QUERY = "select SubCategory from jokes where Category = ";
    private static final String SELECT_JOKES_QUERY = "select * from jokes ";

    private DataBaseHelper mdbHelper;
    public JokesTable(Application myApp) {
        mdbHelper = ((JokesApplication)myApp).getDataHelper();
    }
    public ArrayList<CategoryModel> fetchCategory() {
        ArrayList<CategoryModel> categoryModels = new ArrayList<>();
        try {
            SQLiteDatabase db = mdbHelper.getReadableDatabase();

            Cursor cursor = db.rawQuery(SELECT_CATEGORY_QUERY,null);
            if (cursor != null)
                cursor.moveToFirst();
            assert cursor != null;
            if (cursor.getCount() == 0) {
                return categoryModels;
            } else {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    categoryModels.add(new CategoryModel(cursor.getString(0)));
                    cursor.moveToNext();
                }
                cursor.close();
                return categoryModels;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return categoryModels;
        }
    }
    public ArrayList<JokesModel> fetchJokes(String id,String tag) {
        int position=0;
        ArrayList<JokesModel> categoryModels = new ArrayList<>();
        try {
            Cursor cursor ;
            SQLiteDatabase db = mdbHelper.getReadableDatabase();
            if(tag.equalsIgnoreCase("category"))
            {
                cursor = db.rawQuery(SELECT_JOKES_QUERY+"where Category = "+"'"+id+"'",null);
            }else
            {
                if (id.equalsIgnoreCase(""))
                    cursor = db.rawQuery(SELECT_JOKES_QUERY + "where isFavourited = " + "'" + 1 + "'", null);

                else
                    cursor = db.rawQuery(SELECT_JOKES_QUERY + "where SubCategory = " + "'" + id + "'", null);
            }

            if (cursor != null)
                cursor.moveToFirst();
            assert cursor != null;
            if (cursor.getCount() == 0)
            {
                return categoryModels;
            } else
            {
                cursor.moveToFirst();
                while (!cursor.isAfterLast())
                {
                    categoryModels.add(new JokesModel(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),position));
                    position=position+1;
                    cursor.moveToNext();
                }
                cursor.close();
                return categoryModels;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return categoryModels;
        }
    }

    public ArrayList<String> fetchSubCategory(String category, int seatchType) {
        ArrayList<String> categoryModels = new ArrayList<>();
        try {
            SQLiteDatabase db = mdbHelper.getReadableDatabase();
//            Cursor cursor = db.rawQuery(SELECT_SUBCATEGORY_QUERY+"'"+category+"'",null);
            Cursor cursor = db.rawQuery(SELECT_JOKES_QUERY+"where Category = "+"'"+category+"'",null);

            if (cursor != null)
                cursor.moveToFirst();

            assert cursor != null;
            int listCount = cursor.getCount() ;
            if (listCount == 0) {
                return categoryModels;
            } else {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    categoryModels.add(cursor.getString(0));
                    cursor.moveToNext();
                }

                cursor.close();
                return categoryModels;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return categoryModels;
        }
    }

    public void setFavreteJokes(String id,boolean idSelect)
    {
        try {
            SQLiteDatabase db = mdbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            if (idSelect)
                values.put(IS_FAVOURITED, 1);
            else
                values.put(IS_FAVOURITED, 0);

            db.update(JOKES_TABLE, values,"id="+id, null);
            db.close();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
