package com.m.jokes.db;

import android.app.Application;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.m.jokes.application.JokesApplication;
import com.m.jokes.model.NotesModel;
import java.util.ArrayList;

/**
 * Created by sujeet on 05/06/17.
 *
 */
public class NotesTable {
    private static final String NOTE_TABLE = "notes";
    private static final String NOTE_TEXT = "note_text";
    private static final String NOTE_DATE = "note_date";
    private DataBaseHelper mdbHelper;
    //
    public NotesTable(Application myApp) {
        mdbHelper = ((JokesApplication)myApp).getDataHelper();
    }

    public void insertNote(String noteText, String noteDate) {
        SQLiteDatabase db = mdbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NOTE_TEXT, noteText);
        values.put(NOTE_DATE, noteDate);
        db.insert(NOTE_TABLE, null, values);
        db.close();
    }

    public boolean isNotesAvaialbe(String noteDate)
    {
        SQLiteDatabase db = mdbHelper.getReadableDatabase();

        Cursor cursor = db.query(NOTE_TABLE, new String[]{
                        NOTE_DATE}, noteDate,
                null, null, null,null, null);
        return cursor != null && cursor.getCount() > 0;
    }

    public void deleteNote(String note_date) {
        SQLiteDatabase db = mdbHelper.getWritableDatabase();
        db.delete(NOTE_TABLE, NOTE_DATE + "=" + "'" + note_date + "'", null);
        db.close();
    }

    public ArrayList<NotesModel> fetchNote() {
        ArrayList<NotesModel> notesModels = new ArrayList<>();
        try {
            SQLiteDatabase db = mdbHelper.getReadableDatabase();

            Cursor cursor = db.query(NOTE_TABLE, new String[]{
                            NOTE_TEXT, NOTE_DATE}, null,
                    null, null, null,NOTE_DATE+" DESC", null);
            if (cursor != null)
                cursor.moveToFirst();
            assert cursor != null;
            if (cursor.getCount() == 0) {
                return notesModels;
            } else {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    notesModels.add(new NotesModel(cursor.getString(0), cursor.getString(1)));
                    cursor.moveToNext();
                }
                cursor.close();
                return notesModels;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return notesModels;
        }
    }
}

