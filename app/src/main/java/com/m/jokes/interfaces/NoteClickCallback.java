package com.m.jokes.interfaces;

/**
 * Created by SUJEET on 19-11-2017.
 */

public interface NoteClickCallback {
    void openNote(String noteText, String noteDate,int pos);
}
