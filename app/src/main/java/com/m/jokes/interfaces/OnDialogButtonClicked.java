package com.m.jokes.interfaces;

/**
 * Created by saten on 12/2/17.
 */

public interface OnDialogButtonClicked
{
    void onDialogButtonClicked(int btnId);
    void onDialogButtonClicked(int btnId,String tag);
}

