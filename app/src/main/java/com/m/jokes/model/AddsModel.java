package com.m.jokes.model;

import android.graphics.Bitmap;

/**
 * Created by saten on 12/25/17.
 */

public class AddsModel {
    String addsTitle;
    String addsDecs;
    Bitmap bitmap;

    public AddsModel(String addsTitle, String addsDecs, Bitmap bitmap) {
        this.addsTitle = addsTitle;
        this.addsDecs = addsDecs;
        this.bitmap = bitmap;
    }

    public String getAddsTitle() {
        return addsTitle;
    }

    public void setAddsTitle(String addsTitle) {
        this.addsTitle = addsTitle;
    }

    public String getAddsDecs() {
        return addsDecs;
    }

    public void setAddsDecs(String addsDecs) {
        this.addsDecs = addsDecs;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
