package com.m.jokes.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.startapp.android.publish.ads.nativead.NativeAdDetails;

/**
 * Created by Sujeet on 08-06-2017.
 *
 */

public class JokesModel implements Parcelable {

    private String category;
    private String subCategory;
    private String message;
    private String isFavourited;
    private int id;
    private int position;
    private NativeAdDetails nativeAdDetails;

    public NativeAdDetails getNativeAdDetails() {
        return nativeAdDetails;
    }

    public void setNativeAdDetails(NativeAdDetails nativeAdDetails) {
        this.nativeAdDetails = nativeAdDetails;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getId()
    {
        return id;
    }
    public String getCategory() {
        return category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public String getMessage() {
        return message;
    }

    public void setIsFavourited(String isFavourited)
    {
        this.isFavourited = isFavourited;
    }
    public String getIsFavourited() {
        return isFavourited;
    }

    public JokesModel() {
    }

    public JokesModel(int id ,String category, String subCategory,String message, String isFavourited,int position) {
        this.id = id;
        this.category = category;
        this.subCategory = subCategory;
        this.message = message;
        this.isFavourited = isFavourited;
        this.position=position;

    }

    public JokesModel(Parcel in) {
        id = in.readInt();
        category = in.readString();
        subCategory = in.readString();
        message = in.readString();
        isFavourited = in.readString();
    }

    public static final Creator<JokesModel> CREATOR = new Creator<JokesModel>() {
        @Override
        public JokesModel createFromParcel(Parcel in) {
            return new JokesModel(in);
        }

        @Override
        public JokesModel[] newArray(int size) {
            return new JokesModel[size];
        }
    };
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(category);
        parcel.writeString(subCategory);
        parcel.writeString(message);
        parcel.writeString(isFavourited);
    }
}
