package com.m.jokes.model;

/**
 * Created by saten on 12/3/17.
 */

public class JokesSubCategoryModel {
    String subCategoryName;
    String subCategoryDescription;

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public String getSubCategoryDescription() {
        return subCategoryDescription;
    }

    public void setSubCategoryDescription(String subCategoryDescription) {
        this.subCategoryDescription = subCategoryDescription;
    }
}
