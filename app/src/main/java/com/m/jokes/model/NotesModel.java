package com.m.jokes.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Sujeet on 08-06-2017.
 *
 */

public class NotesModel implements Parcelable {

    private String note_text;
    private String note_date;

    public NotesModel() {
    }

    public NotesModel(String note_text, String note_date) {
        this.note_text = note_text;
        this.note_date = note_date;
    }

    protected NotesModel(Parcel in) {
        note_text = in.readString();
        note_date = in.readString();
    }

    public static final Creator<NotesModel> CREATOR = new Creator<NotesModel>() {
        @Override
        public NotesModel createFromParcel(Parcel in) {
            return new NotesModel(in);
        }

        @Override
        public NotesModel[] newArray(int size) {
            return new NotesModel[size];
        }
    };

    public String getNote_text() {
        return note_text;
    }

    public void setNote_text(String note_text) {
        this.note_text = note_text;
    }

    public String getNote_date() {
        return note_date;
    }

    public void setNote_date(String note_date) {
        this.note_date = note_date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(note_text);
        parcel.writeString(note_date);
    }
}
