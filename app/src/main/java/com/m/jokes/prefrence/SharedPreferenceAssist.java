package com.m.jokes.prefrence;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * This class is used to just initialize the shared preferenece.
 *
 * @author shubham.gupta
 */
public class SharedPreferenceAssist {

    private static final String PREFS_FILE_NAME = "JokesApp";

    private static SharedPreferences mSharedPreference;

    public static void initialize(Context pContext) {
        mSharedPreference = pContext.getSharedPreferences(PREFS_FILE_NAME, Context.MODE_PRIVATE);
    }

    public static SharedPreferences getSharedPreference() {
        return mSharedPreference;
    }

    /**
     * This function will be used when user logout himself from the app. Then
     * this method will delete all the keys saved in the shared preference.
     * <p>
     * return
     */
    public static boolean clearAllSharedPreference() {
        Editor editor = mSharedPreference.edit();
        editor.clear();
        return editor.commit();
    }
}
