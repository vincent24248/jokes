package com.m.jokes.services;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import com.m.jokes.R;
import com.m.jokes.application.JokesApplication;
import com.m.jokes.db.JokesTable;
import com.m.jokes.model.CategoryModel;
import com.m.jokes.model.JokesModel;
import com.m.jokes.prefrence.AppSharedPreference;
import com.m.jokes.prefrence.PrefConstants;
import com.m.jokes.receiver.AlarmReceiver;
import com.m.jokes.ui.activity.JokeDetailsActivity;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import static com.m.jokes.constant.AppConstant.ACTION_LOCAL_PUSH_COUNT;
import static com.m.jokes.constant.AppConstant.SELECTED_POSITION;
import static com.m.jokes.constant.AppConstant.SUBCATEGORY_NAME;
import static com.m.jokes.utils.Constants.FROM_NOTIFICATION;
import static com.m.jokes.utils.Constants.IS_COMING_FROM;

/**
 * Created by saten on 2/10/18.
 */

public class JokesJobService extends JobService {
    private static final String TAG = "SyncService";

    @Override
    public boolean onStartJob(JobParameters params) {
        AppSharedPreference.putInt(PrefConstants.RECEVED_NOTIFICATION_COUNT, AppSharedPreference.getInt(PrefConstants.RECEVED_NOTIFICATION_COUNT, 0, JokesApplication.getInstance())+1, JokesApplication.getInstance());
    JokesModel jokesModel=getRandomJoke();
    String categoryName=jokesModel.getCategory();
    String  subCategoryName=jokesModel.getSubCategory();
    String jokesContent=jokesModel.getMessage();
    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext()).setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.ic_launcher)).setSmallIcon(R.drawable.ic_launcher).setContentTitle(subCategoryName).setColor(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? getApplicationContext().getResources().getColor(R.color.colorPrimary) : getApplicationContext().getResources().getColor(R.color.transparent)).setStyle(new NotificationCompat.BigTextStyle().bigText(subCategoryName)).setContentText(jokesContent);
    Intent resultIntent = new Intent(getApplicationContext(), JokeDetailsActivity.class);
    resultIntent.putExtra(IS_COMING_FROM,FROM_NOTIFICATION);
    resultIntent.putExtra(SELECTED_POSITION,jokesModel.getPosition());
    resultIntent.putExtra(SUBCATEGORY_NAME,jokesModel.getCategory());
    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
    PendingIntent contentIntent = PendingIntent.getActivity(JokesApplication.getInstance(), 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    mBuilder.setContentIntent(contentIntent);
    mBuilder.setAutoCancel(true);
    mBuilder.setDefaults(NotificationCompat.DEFAULT_ALL);
    NotificationManager mNotificationManager = (NotificationManager)getApplicationContext(). getSystemService(Context.NOTIFICATION_SERVICE);
    mNotificationManager.notify(AppSharedPreference.getInt(PrefConstants.RECEVED_NOTIFICATION_COUNT, 0, JokesApplication.getInstance()), mBuilder.build());
    if(android.os.Build.VERSION.SDK_INT> Build.VERSION_CODES.LOLLIPOP_MR1)
    {
        AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(getApplicationContext().ALARM_SERVICE);
        Calendar calendarFuture = Calendar.getInstance();
        Calendar calendaToday = Calendar.getInstance();
        int hourOfTheDay=calendaToday.get(Calendar.HOUR_OF_DAY);
        calendarFuture.set(Calendar.HOUR_OF_DAY,hourOfTheDay+1);
        calendarFuture.set(Calendar.MINUTE, 0);
        calendarFuture.set(Calendar.SECOND, 0);

        Intent mintent = new Intent(getApplicationContext(), AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, mintent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager.AlarmClockInfo alarmClockInfo=new AlarmManager.AlarmClockInfo(calendarFuture.getTimeInMillis(),pendingIntent);
        alarmManager.setAlarmClock(alarmClockInfo,  pendingIntent);
    }
        return true;


}

    private JokesModel getRandomJoke()
    {
        ArrayList<CategoryModel> categorModelList = new ArrayList<>();
        JokesTable jokesTable = new JokesTable(JokesApplication.getInstance());
        categorModelList = jokesTable.fetchCategory();
        Random r = new Random();
        int categoryPosition=r.nextInt(categorModelList.size());
        CategoryModel categoryModel=categorModelList.get(categoryPosition);
        String jokTitle = categoryModel.getCategory();
        ArrayList<JokesModel>  jokesList = jokesTable.fetchJokes(jokTitle,"category");
        int subcategoryPoition=r.nextInt(jokesList.size());
        JokesModel jokesModel=jokesList.get(subcategoryPoition);
        return jokesModel;

    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }

}