package com.m.jokes.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.appnext.ads.fullscreen.FullScreenVideo;
import com.appnext.base.Appnext;
import com.appnext.core.AppnextError;
import com.appnext.core.ECPM;
import com.appnext.core.callbacks.OnAdClicked;
import com.appnext.core.callbacks.OnAdClosed;
import com.appnext.core.callbacks.OnAdError;
import com.appnext.core.callbacks.OnAdLoaded;
import com.appnext.core.callbacks.OnAdOpened;
import com.appnext.core.callbacks.OnECPMLoaded;
import com.appnext.core.callbacks.OnVideoEnded;
import com.m.jokes.R;
import static com.m.jokes.utils.Constants.AppNext;

public class ActivityAddVideo extends AppCompatActivity {
    FullScreenVideo fullscreen_ad;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_of_the_day);
        // Toast.makeText(ActivityAddVideo.this,"fullscreen_ad",Toast.LENGTH_SHORT).show();
        progressBar=(ProgressBar)findViewById(R.id.progressbar);
        progressBar.setVisibility(View.VISIBLE);
        Appnext.init(this);
         fullscreen_ad = new FullScreenVideo(this, AppNext);
        fullscreen_ad.loadAd();
        // Get callback for ad loaded
        fullscreen_ad.setOnAdLoadedCallback(new OnAdLoaded() {
            @Override
            public void adLoaded(String bannerID) {
                if (fullscreen_ad.isAdLoaded())
                {
                    progressBar.setVisibility(View.GONE);
                    fullscreen_ad.showAd();

                }else{
                    progressBar.setVisibility(View.GONE);
                    //continue...
                }
            }
        });

// Get callback for ad opened
        fullscreen_ad.setOnAdOpenedCallback(new OnAdOpened() {
            @Override
            public void adOpened() {

            }
        });

// Get callback for ad clicked
        fullscreen_ad.setOnAdClickedCallback(new OnAdClicked() {
            @Override
            public void adClicked() {

            }
        });

// Get callback for ad closed
        fullscreen_ad.setOnAdClosedCallback(new OnAdClosed() {
            @Override
            public void onAdClosed() {
              finish();
            }
        });

// Get callback for ad error

        fullscreen_ad.setOnAdErrorCallback(new OnAdError() {
            @Override
            public void adError(String error) {
                switch (error){
                    case AppnextError.NO_ADS:
                        Log.e("appnext", "no ads");
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(ActivityAddVideo.this,"No Adds Available",Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                    case AppnextError.CONNECTION_ERROR:
                        Log.e("appnext", "connection problem");
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(ActivityAddVideo.this,"Connection problem",Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                    default:
                        Log.e("appnext", "other error");
                        Toast.makeText(ActivityAddVideo.this,"Connection error",Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);
                        finish();
                }
            }
        });
// Get callback when the user saw the video until the end (video ended)
        fullscreen_ad.setOnVideoEndedCallback(new OnVideoEnded() {
            @Override
            public void videoEnded() {
                finish();

            }
        });

    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
