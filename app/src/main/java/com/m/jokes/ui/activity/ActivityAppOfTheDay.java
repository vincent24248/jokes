package com.m.jokes.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.appnext.ads.AdsError;
import com.appnext.ads.fullscreen.RewardedVideo;
import com.appnext.ads.interstitial.Interstitial;
import com.appnext.base.Appnext;
import com.appnext.core.AppnextError;
import com.appnext.core.callbacks.OnAdClicked;
import com.appnext.core.callbacks.OnAdClosed;
import com.appnext.core.callbacks.OnAdError;
import com.appnext.core.callbacks.OnAdLoaded;
import com.appnext.core.callbacks.OnAdOpened;
import com.appnext.core.callbacks.OnVideoEnded;
import com.m.jokes.R;

import static com.m.jokes.utils.Constants.AppNext;

public class ActivityAppOfTheDay extends AppCompatActivity {
    Interstitial interstitial_Ad  ;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_of_the_day);
       // Toast.makeText(ActivityAppOfTheDay.this,"rewarded_ad",Toast.LENGTH_SHORT).show();
        progressBar=(ProgressBar)findViewById(R.id.progressbar);
        progressBar.setVisibility(View.VISIBLE);
        Appnext.init(this);
         interstitial_Ad = new Interstitial(this, AppNext);
        interstitial_Ad.loadAd();
        // Get callback for ad loaded

        interstitial_Ad.setOnAdLoadedCallback(new OnAdLoaded() {
            @Override
            public void adLoaded(String s)
            {
                interstitial_Ad.showAd();
                progressBar.setVisibility(View.GONE);
            }
        });

        interstitial_Ad.setOnAdOpenedCallback(new OnAdOpened() {
            @Override
            public void adOpened() {

            }
        });// Get callback for ad clicked
        interstitial_Ad.setOnAdClickedCallback(new OnAdClicked() {
            @Override
            public void adClicked() {

            }
        });// Get callback for ad closed
        interstitial_Ad.setOnAdClosedCallback(new OnAdClosed() {
            @Override
            public void onAdClosed() {
                finish();

            }
        });// Get callback for ad error
        interstitial_Ad.setOnAdErrorCallback(new OnAdError() {
            @Override
            public void adError(String error)
            {
                Toast.makeText(ActivityAppOfTheDay.this,error,Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        interstitial_Ad.setOnAdErrorCallback(new OnAdError() {
            @Override
            public void adError(String error) {
                switch (error){
                    case AdsError.NO_ADS:
                        Toast.makeText(ActivityAppOfTheDay.this,error,Toast.LENGTH_SHORT).show();
                        Log.v("appnext", "no ads");
                        break;
                    case AdsError.CONNECTION_ERROR:
                        Toast.makeText(ActivityAppOfTheDay.this,error,Toast.LENGTH_SHORT).show();
                        Log.v("appnext", "connection problem");
                        break;
                    default:
                        Toast.makeText(ActivityAppOfTheDay.this,error,Toast.LENGTH_SHORT).show();
                        Log.v("appnext", "other error");
                }
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
