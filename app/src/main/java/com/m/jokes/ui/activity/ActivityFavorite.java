package com.m.jokes.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.m.jokes.R;
import com.m.jokes.adapter.FavoriteJokesAdapter;
import com.m.jokes.application.JokesApplication;
import com.m.jokes.base.BaseActivity;
import com.m.jokes.db.JokesTable;
import com.m.jokes.model.JokesModel;
import com.m.jokes.prefrence.AppSharedPreference;
import com.m.jokes.prefrence.PrefConstants;
import com.m.jokes.utils.Utils;
import com.startapp.android.publish.ads.nativead.NativeAdDetails;
import com.startapp.android.publish.ads.nativead.NativeAdPreferences;
import com.startapp.android.publish.ads.nativead.StartAppNativeAd;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import java.util.ArrayList;
import java.util.Iterator;

import static com.m.jokes.utils.Constants.POSITION_RIGHT_BOTTOM;

public class ActivityFavorite extends BaseActivity
{
    private FavoriteJokesAdapter favoriteJokesAdapter ;
    ArrayList<JokesModel> jokesModelArrayList=new ArrayList<>();
    ArrayList<NativeAdDetails>  addsModels=new ArrayList<>();
    private JokesTable jokesTable ;
    private RecyclerView rv_Category ;
    TextView lblNoDataFound;
    NativeAdDetails nativeDetail;
      int addsRequireToLoad=1;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite);
        Toolbar toolbar = (Toolbar) findViewById(R.id.Categor_toolbar);
        manageToolBar(toolbar,"Favorite");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        rv_Category = (RecyclerView) findViewById(R.id.favorite_item_list);
        lblNoDataFound=(TextView)findViewById(R.id.lbl_no_data_found);
        if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE,false,ActivityFavorite.this))
        {
            Utils.showPollifishAddRight(ActivityFavorite.this,POSITION_RIGHT_BOTTOM);

        }
        getFavoriteList();
        configNativeStartAppAds();

    }


    private void getFavoriteList()
    {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ActivityFavorite.this);
        favoriteJokesAdapter = new FavoriteJokesAdapter(ActivityFavorite.this, getFavouritelist());
        rv_Category.setLayoutManager(linearLayoutManager);
        rv_Category.setAdapter(favoriteJokesAdapter);

    }

    @Override
    protected void onResume()
    {
        super.onResume();
        favoriteJokesAdapter.setAdapterData(getFavouritelist(),addsModels);
        setLblNoDatFound();
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btn_back:
                finish();
                break;
        }
    }
    public ArrayList<JokesModel> getFavouritelist()
    {
        jokesModelArrayList=new ArrayList<>();
        jokesTable = new JokesTable(getApplication());
         ArrayList<JokesModel>  favJokesTableList=jokesTable.fetchJokes("","");
        addsRequireToLoad=favJokesTableList.size();
     /* if(favJokesTableList!=null&&favJokesTableList.size()>0)
      {
          for (int i = 0; i < favJokesTableList.size(); i++)
          {
              if (!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE, false, JokesApplication.getInstance()))
              {
                  if (i % 3 == 0)
                  {
                      jokesModelArrayList.add(null);
                  }
              }
              jokesModelArrayList.add(favJokesTableList.get(i));

          }
      }
     if(addsModels!=null&&addsModels.size()>0)
     {

    }else
    {*/
      jokesModelArrayList.addAll(favJokesTableList);

     //}
        return  jokesModelArrayList;
    }
    void setLblNoDatFound()
    {
        if(jokesModelArrayList==null||jokesModelArrayList.size()==0)
        {
            lblNoDataFound.setVisibility(View.VISIBLE);
            rv_Category.setVisibility(View.GONE);
        }else
        {
            lblNoDataFound.setVisibility(View.GONE);
            rv_Category.setVisibility(View.VISIBLE);
        }
    }

    private void configNativeStartAppAds() {
        final StartAppNativeAd startAppNativeAd = new StartAppNativeAd(ActivityFavorite.this);

        Log.e("addsSizeFavToLoad ",""+addsRequireToLoad);
        int addsToLoad=(addsRequireToLoad<=0)?1:addsRequireToLoad;
        NativeAdPreferences nativePrefs = new NativeAdPreferences()
                .setAdsNumber(addsToLoad)                // Load 3 Native Ads
                .setAutoBitmapDownload(true)    // Retrieve Images object
                .setPrimaryImageSize(0);        // 150x150 image
        //To load your native ad, call the loadAd() method with a NativeAdPreferences object:

        startAppNativeAd.loadAd(nativePrefs, new AdEventListener()
        {
            @Override
            public void onReceiveAd(Ad arg0)
            {
                ArrayList<NativeAdDetails> ads = startAppNativeAd.getNativeAds();    // get NativeAds list
                // Print all ads details to log
                Iterator<NativeAdDetails> iterator = ads.iterator();
                while(iterator.hasNext())
                {
                    nativeDetail=   iterator.next();
                    addsModels.add(nativeDetail);

                }
                if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE, false, JokesApplication.getInstance())&&addsModels!=null&&addsModels.size()>0)
                {
                    favoriteJokesAdapter.setAdapterData(getFavouritelist(),addsModels);
                }
                Log.e("addsSizeFav ",""+addsModels.size());
            }

            @Override
            public void onFailedToReceiveAd(Ad arg0)
            {

                //  Toast.makeText(getActivity(),"onFailedToReceiveAd",Toast.LENGTH_SHORT).show();
                // Native Ad failed to receive
            }
        });
    }

}
