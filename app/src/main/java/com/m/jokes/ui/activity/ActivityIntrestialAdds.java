package com.m.jokes.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;
import com.m.jokes.R;
import com.m.jokes.utils.Utils;

public class ActivityIntrestialAdds extends AppCompatActivity {
    InterstitialAd interstitialAd;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adds_free);
        progressBar=(ProgressBar)findViewById(R.id.progress_bar);
        interstitialAd= Utils.intrestialAddbject(ActivityIntrestialAdds.this);
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
              //  Toast.makeText(ActivityIntrestialAdds.this,"onAdLoaded",Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
                interstitialAd.show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                progressBar.setVisibility(View.GONE);
                // Code to be executed when an ad request fails.
                //  Toast.makeText(ActivityIntrestialAdds.this,"onAdFailedToLoad",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                progressBar.setVisibility(View.GONE);
                // Code to be executed when the ad is displayed.
                // Toast.makeText(ActivityIntrestialAdds.this,"onAdOpened",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                progressBar.setVisibility(View.GONE);
                // Code to be executed when the user has left the app.
                //  Toast.makeText(ActivityIntrestialAdds.this,"onAdLeftApplication",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdClosed() {
                // Toast.makeText(ActivityIntrestialAdds.this,"onAdClosed",Toast.LENGTH_SHORT).show();
                finish();
                // Code to be executed when when the interstitial ad is closed.
            }
        });


    }

    @Override
    protected void onResume()
    {
        super.onResume();
       // Utils.showPollifishAddRight(POSITION_RIGHT_TOP);

    }


}
