package com.m.jokes.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.google.android.gms.ads.AdView;
import com.m.jokes.R;
import com.m.jokes.application.JokesApplication;
import com.m.jokes.db.NotesTable;
import com.m.jokes.prefrence.AppSharedPreference;
import com.m.jokes.prefrence.PrefConstants;
import com.m.jokes.utils.Utils;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import static com.m.jokes.utils.Constants.POSITION_RIGHT_BOTTOM;

public class AddNoteActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private EditText et_new_note;
    private NotesTable jokesDb;
    private String currentDateTime;
    private TextView currentTime;
    private String fetchedDateTime = "";
    private String fetchedText = "";
    private boolean NOT_DELETED = true;
    private boolean NOT_CHANGED = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);
        toolbar = (Toolbar) findViewById(R.id.notes_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add Notes");
        getSystemTime();
        Intent intent = getIntent();
        currentTime = (TextView) findViewById(R.id.current_time);
        currentTime.setText(currentDateTime);
        jokesDb = new NotesTable(getApplication());
        et_new_note = (EditText) findViewById(R.id.et_add_notes);
        toolbar = (Toolbar) findViewById(R.id.notes_toolbar);

        if (intent != null && intent.getExtras() != null) {
            if (intent.getExtras().get("text") != null && intent.getExtras().get("date") != null) {
                getSupportActionBar().setTitle("Edit Notes");
                NOT_CHANGED = false;
                fetchedDateTime = intent.getExtras().get("date").toString();
                fetchedText = intent.getExtras().get("text").toString();
                et_new_note.setText(fetchedText);
                et_new_note.setSelection(fetchedText.length());
                currentTime.setText(fetchedDateTime);
            }/*else
                Utils.showNoteDialog(this,"You can write a note or dictate it by clicking on the Mic icon in the keyboard");
        }else
        Utils.showNoteDialog(this,"You can write a note or dictate it by clicking on the Mic icon in the keyboard");*/
        }
        if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE, false, JokesApplication.getInstance()))
        {
            AdView adView = (AdView) findViewById(R.id.adView);
            adView.setVisibility(View.VISIBLE);
            adView.loadAd(Utils.getAdRefence(AddNoteActivity.this));
        }

    }


    private void getSystemTime() {
        //date output format
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        currentDateTime = dateFormat.format(cal.getTime());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_note_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.share_note:
                onBackPressed();

                break;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //add note
        if (!(fetchedText.equals(et_new_note.getText().toString().trim()))) {
            NOT_CHANGED = false;
        }
        if (!(et_new_note.getText().toString().trim().equals("")) && NOT_DELETED) {
            if (fetchedText.equals("")) {
                jokesDb.insertNote(et_new_note.getText().toString(), currentDateTime);
            } else if (!NOT_CHANGED) {
                jokesDb.deleteNote(fetchedDateTime);
                jokesDb.insertNote(et_new_note.getText().toString(), currentDateTime);
            }
        }
        if (!NOT_DELETED || !NOT_CHANGED)
            setResult(203);
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
      //  Utils.showPollifishAddRight(AddNoteActivity.this,POSITION_RIGHT_BOTTOM);

    }
}
