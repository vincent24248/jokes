package com.m.jokes.ui.activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.m.jokes.R;
import com.m.jokes.adapter.CategoryAdapter;
import com.m.jokes.base.BaseActivity;
import com.m.jokes.db.JokesTable;
import com.m.jokes.model.CategoryModel;
import com.m.jokes.utils.Utils;
import java.util.ArrayList;
import java.util.Iterator;

import static com.m.jokes.utils.Constants.POSITION_RIGHT_BOTTOM;
import  com.startapp.android.publish.ads.nativead.*;
import  com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import  com.startapp.android.publish.adsCommon.Ad;
import  com.startapp.android.*;

public class CategoryActivity extends BaseActivity {
    
    private ArrayList<CategoryModel> CategorModelList;
    private CategoryAdapter CategorAdapter;
    private JokesTable salesDb;
    private RecyclerView rv_Category ;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        Toolbar toolbar = (Toolbar) findViewById(R.id.Categor_toolbar);
        manageToolBar(toolbar,"Category");
        rv_Category = (RecyclerView) findViewById(R.id.Categor_list);
        getCategor();
    }

    private void getCategor()
    {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CategoryActivity.this);
        CategorModelList = new ArrayList<>();
        salesDb = new JokesTable(getApplication());
        CategorModelList = salesDb.fetchCategory();
        CategorAdapter = new CategoryAdapter(CategoryActivity.this, CategorModelList);
        rv_Category.setLayoutManager(linearLayoutManager);
        rv_Category.setAdapter(CategorAdapter);
        rv_Category.scrollToPosition(0);
       // rv_Category.addItemDecoration(new SpaceItemDecoration(5, false));
    }
    @Override
    protected void onResume() {
        super.onResume();
       // Utils.showPollifishAddRight(CategoryActivity.this,POSITION_RIGHT_BOTTOM);

    }

    private void configNativeStartAppAds() {
       final StartAppNativeAd startAppNativeAd = new StartAppNativeAd(this);
        NativeAdPreferences nativePrefs = new NativeAdPreferences()
                .setAdsNumber(1)                // Load 3 Native Ads
                .setAutoBitmapDownload(true)    // Retrieve Images object
                .setPrimaryImageSize(3);        // 150x150 image
        //To load your native ad, call the loadAd() method with a NativeAdPreferences object:

        startAppNativeAd.loadAd(nativePrefs, new AdEventListener() {
            @Override
            public void onReceiveAd(Ad arg0) {
                ArrayList<NativeAdDetails> ads = startAppNativeAd.getNativeAds();    // get NativeAds list
                // Print all ads details to log
                Iterator<NativeAdDetails> iterator = ads.iterator();
                while(iterator.hasNext()){
                    NativeAdDetails nativeDetail = iterator.next();
                    nativeDetail.getCategory();
                    nativeDetail.getImageBitmap();
                    nativeDetail.getDescription();
                    nativeDetail.getTitle();
                    nativeDetail.getImageUrl();
                    break;
                }
            }

            @Override
            public void onFailedToReceiveAd(Ad arg0) {
                // Native Ad failed to receive
            }
        });
    }
}
