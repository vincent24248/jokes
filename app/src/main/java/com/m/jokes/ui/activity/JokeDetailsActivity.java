package com.m.jokes.ui.activity;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;
import com.m.jokes.R;
import com.m.jokes.adapter.JokeDetailsAdapter;
import com.m.jokes.application.JokesApplication;
import com.m.jokes.base.BaseActivity;
import com.m.jokes.db.JokesTable;
import com.m.jokes.model.JokesModel;
import com.m.jokes.prefrence.AppSharedPreference;
import com.m.jokes.prefrence.PrefConstants;
import com.m.jokes.utils.Utils;
import com.startapp.android.publish.ads.nativead.NativeAdDetails;
import com.startapp.android.publish.ads.nativead.NativeAdPreferences;
import com.startapp.android.publish.ads.nativead.StartAppNativeAd;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import static com.m.jokes.constant.AppConstant.SELECTED_POSITION;
import static com.m.jokes.constant.AppConstant.SUBCATEGORY_NAME;
import static com.m.jokes.utils.Constants.DEFAULT_TEXT_SIZE;
import static com.m.jokes.utils.Constants.FROM_FAVOURITE;
import static com.m.jokes.utils.Constants.FROM_NOTIFICATION;
import static com.m.jokes.utils.Constants.FROM_SUBCATEGORY;
import static com.m.jokes.utils.Constants.IS_COMING_FROM;

public class JokeDetailsActivity extends BaseActivity {
    private String jokTitle ;
    private ImageView imgFavorite;
    private JokesTable jokesTable ;
    private ArrayList<JokesModel> jokesList ;
    int clickCount=0;
    ViewPager viewPager ;
    JokeDetailsAdapter mViewPagerAdapter ;
    private int focusedPage = 0;
   // private TextToSpeech textToSpeech;
    private  NativeAdDetails  nativeDetail;

  //  private ImageView addImg;
  //  private ScrollView addsRelay;
    //private TextView txtTitle,txtDescription;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joke_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.htab_toolbar);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
       // addsRelay=(ScrollView)findViewById(R.id.ads_relay);
       // txtTitle=(TextView)findViewById(R.id.text_title);
       // txtDescription=(TextView)findViewById(R.id.txt_description);
        manageToolBar(toolbar,"");
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                finish();
            }
        });
        AppSharedPreference.putInt(PrefConstants.RECEVED_NOTIFICATION_COUNT,0, JokesApplication.getInstance());
        initView();
        configNativeStartAppAds();
       /* textToSpeech=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener()
        {
            @Override
            public void onInit(int status)
            {
                if(status != TextToSpeech.ERROR)
                {
                    textToSpeech.setLanguage(Locale.UK);
                }
            }
        });
*/
    }




    private void initView() {
        imgFavorite=(ImageView)findViewById(R.id.img_fev);
        findViewById(R.id.btn_next).setOnClickListener(this);
        findViewById(R.id.btn_prev).setOnClickListener(this);
        findViewById(R.id.btn_fev).setOnClickListener(this);
        findViewById(R.id.btn_speak).setOnClickListener(this);
        findViewById(R.id.btn_sufful).setOnClickListener(this);
       // addImg=(ImageView)findViewById(R.id.add_img);
      //  addImg.setOnClickListener(this);
           try {
         getData();
        }catch (Exception e)
        {
         e.printStackTrace();
         }

    }

    private void getData() {
        jokesTable = new JokesTable(getApplication());
        if(getIntent().getIntExtra(IS_COMING_FROM,0)==FROM_FAVOURITE)
        {
            focusedPage=getIntent().getIntExtra(SELECTED_POSITION,0);
            jokesList = jokesTable.fetchJokes("","");
            if (jokesList.size() > 0)
            {
                JokesModel jokesModel = jokesList.get(focusedPage);
                setFavIcon(jokesModel.getIsFavourited());

            }
        }else if(getIntent().getIntExtra(IS_COMING_FROM,0)==FROM_SUBCATEGORY)
        {
            focusedPage=getIntent().getIntExtra(SELECTED_POSITION,0);
            jokTitle = getIntent().getStringExtra(SUBCATEGORY_NAME);
            jokesList = jokesTable.fetchJokes(jokTitle,"category");
            if (jokesList.size() > 0)
            {
                JokesModel jokesModel = jokesList.get(focusedPage);
                setFavIcon(jokesModel.getIsFavourited());
            }
        }else if(getIntent().getIntExtra(IS_COMING_FROM,0)==FROM_NOTIFICATION)
        {
            focusedPage=getIntent().getIntExtra(SELECTED_POSITION,0);
            jokTitle = getIntent().getStringExtra(SUBCATEGORY_NAME);
            jokesList = jokesTable.fetchJokes(jokTitle,"category");
            if (jokesList.size() > 0)
            {
                JokesModel jokesModel = jokesList.get(focusedPage);
                setFavIcon(jokesModel.getIsFavourited());
            }
        }

        mViewPagerAdapter = new JokeDetailsAdapter(this, jokesList);
        viewPager.setAdapter(mViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        viewPager.setCurrentItem(focusedPage);
    }



    private void shareText(String text) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, text+" https://play.google.com/store/apps/details?id=com.jokes.informative.app");
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Jokes");
        startActivity(Intent.createChooser(intent, "Share"));
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    private void copyText(String text) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Copied Text", text);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(JokeDetailsActivity.this,"copied",Toast.LENGTH_SHORT).show();
    }

    private void moveToPrevious()
    {
        if (focusedPage>0) {

            if(focusedPage>(jokesList.size()-1))
                focusedPage=(jokesList.size()-1);
            setData(--focusedPage);
        }
        else
            Utils.showNoteDialog(this,"Not Available");
    }

    private void moveToNext()
    {

        if (focusedPage<jokesList.size()-1) {
            setData(++focusedPage);
        }
        else
            Utils.showNoteDialog(this,"Not Available");
    }

    private void setData(int pos) {
        try
        {
            viewPager.setCurrentItem(pos,true);
            setFavIcon(jokesList.get(pos).getIsFavourited());
        }catch (Exception e)
        {
            focusedPage=pos-1;
            setData(focusedPage);
            e.printStackTrace();
        }

    }

    private void setFavIcon(String favorite) {
        if (favorite.equalsIgnoreCase("1")) {

            if (Build.VERSION.SDK_INT >= 21) {
                imgFavorite.setImageDrawable(getResources().getDrawable(R.drawable.save_btn, getTheme()));
            } else {
                imgFavorite.setImageDrawable(getResources().getDrawable(R.drawable.save_btn));
            }
        } else {
            if (Build.VERSION.SDK_INT >= 21) {
                imgFavorite.setImageDrawable(getResources().getDrawable(R.drawable.favorite_add, getTheme()));
            } else {
                imgFavorite.setImageDrawable(getResources().getDrawable(R.drawable.favorite_add));
            }
        }
    }


    void openInterestialAddScreen()
    {
        if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE, false, JokesApplication.getInstance()))
        {
        clickCount=clickCount+1;
        if(clickCount>=5) {
            clickCount = 0;
            initialiseFiveClickAdd(JokeDetailsActivity.this);
           /* Intent mIntent = new Intent(JokeDetailsActivity.this, ActivityIntrestialAdds.class);
            startActivity(mIntent);*/

        }
        }
    }
    private void startTextToSpeech()
    {
        Toast.makeText(JokeDetailsActivity.this,"Reading for You",Toast.LENGTH_SHORT).show();
        JokesApplication.getTextToSpeechObj().speak(jokesList.get(focusedPage).getMessage(), TextToSpeech.QUEUE_FLUSH, null);


    }


    @Override
    protected void onResume() {
        super.onResume();
       // Utils.showPollifishAddRight(JokeDetailsActivity.this,POSITION_RIGHT_TOP);

    }



    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position)
        {
           /* View view = mViewPagerAdapter.getAdapterView();
            TextView textView  = (TextView) view.findViewById(R.id.tv_total_item);
            textView.setText(position+1+"/"+jokesList.size());*/

            focusedPage=position;
            setTextViewSize();
            setFavIcon(jokesList.get(focusedPage).getIsFavourited());
            openInterestialAddScreen();

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_manage_jokes, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_copy:
                copyText(jokesList.get(focusedPage).getSubCategory()+"\n"+jokesList.get(focusedPage).getMessage());
                break;
            case R.id.item_share:
                shareText(jokesList.get(focusedPage).getSubCategory()+"\n"+jokesList.get(focusedPage).getMessage());
                break;
            case R.id.item_increase:
                try {

                    AppSharedPreference.putInt(PrefConstants.TEXT_SIZE, AppSharedPreference.getInt(PrefConstants.TEXT_SIZE, DEFAULT_TEXT_SIZE, JokesApplication.getInstance())+1, JokesApplication.getInstance());
                    setTextViewSize();
                }catch (Exception e)
                {
                    e.printStackTrace();
                }

                break;
            case R.id.item_decrease:
                if (AppSharedPreference.getInt(PrefConstants.TEXT_SIZE, DEFAULT_TEXT_SIZE, JokesApplication.getInstance())>DEFAULT_TEXT_SIZE)
                {
                    try {
                        AppSharedPreference.putInt(PrefConstants.TEXT_SIZE, AppSharedPreference.getInt(PrefConstants.TEXT_SIZE, DEFAULT_TEXT_SIZE, JokesApplication.getInstance())-1, JokesApplication.getInstance());
                        setTextViewSize();
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }

                break;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId())
        {
            case R.id.btn_next:
                moveToNext();
                //openInterestialAddScreen();
                break;
            case R.id.btn_prev:
                moveToPrevious();
              //  openInterestialAddScreen();
                break;
            case R.id.btn_fev:
                if (jokesList.get(focusedPage).getIsFavourited().equalsIgnoreCase("1"))
                {
                    jokesTable.setFavreteJokes("" + jokesList.get(focusedPage).getId(), false);
                    jokesList.get(focusedPage).setIsFavourited("0");
                    setFavIcon("0");
                    Toast.makeText(JokeDetailsActivity.this,"Removed from Favorite",Toast.LENGTH_SHORT).show();

                }
                else
                {
                    Toast.makeText(JokeDetailsActivity.this,"Added to Favorite",Toast.LENGTH_SHORT).show();
                    jokesTable.setFavreteJokes("" + jokesList.get(focusedPage).getId(), true);
                    jokesList.get(focusedPage).setIsFavourited("1");
                    setFavIcon("1");
                }
                break;
            case R.id.btn_speak:
                startTextToSpeech();
                break;
            case R.id.btn_sufful:
                Random r = new Random();
                setData(r.nextInt(jokesList.size()));
               // openInterestialAddScreen();
                break;
          /*  case R.id.add_img:
                if(nativeDetail!=null)
                nativeDetail.sendClick(JokeDetailsActivity.this);
                break;*/

        }
    }
    void setTextViewSize()
    {
        View view1 = (View)viewPager.findViewWithTag("view" + focusedPage);
        TextView textView  = (TextView) view1.findViewById(R.id.tv_details);
        textView.setTextSize(AppSharedPreference.getInt(PrefConstants.TEXT_SIZE, DEFAULT_TEXT_SIZE, JokesApplication.getInstance()));

    }

    @Override
    protected void onDestroy()
    {
        if(JokesApplication.getTextToSpeechObj() !=null)
        {
            JokesApplication.getTextToSpeechObj().stop();
            //textToSpeech.shutdown();
        }
        super.onDestroy();

    }

    private void configNativeStartAppAds()
    {
        final StartAppNativeAd startAppNativeAd = new StartAppNativeAd(this);
        final NativeAdPreferences nativePrefs = new NativeAdPreferences()
                .setAdsNumber(1)                // Load 3 Native Ads
                .setAutoBitmapDownload(true)    // Retrieve Images object
                .setPrimaryImageSize(3);        // 150x150 image
        //To load your native ad, call the loadAd() method with a NativeAdPreferences object:

        startAppNativeAd.loadAd(nativePrefs, new AdEventListener()
        {
            @Override
            public void onReceiveAd(Ad arg0)
            {
                ArrayList<NativeAdDetails> ads = startAppNativeAd.getNativeAds();    // get NativeAds list
                Iterator<NativeAdDetails> iterator = ads.iterator();
                while(iterator.hasNext())
                {
                    nativeDetail=   iterator.next();
                    if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE, false, JokesApplication.getInstance()))
                    {
                        mViewPagerAdapter.setAddsData(nativeDetail);

                    }
                    break;
                }
            }

            @Override
            public void onFailedToReceiveAd(Ad arg0)
            {

            }
        });
    }
    public void initialiseFiveClickAdd(Context mContext)
    {
        final InterstitialAd interstitialAd= Utils.intrestialAddbject(mContext);
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded()
            {
                interstitialAd.show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
            }

            @Override
            public void onAdOpened() {

            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdClosed() {

            }
        });

    }
}
