package com.m.jokes.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.ads.AdView;
import com.m.jokes.R;
import com.m.jokes.adapter.NotesAdapter;
import com.m.jokes.application.JokesApplication;
import com.m.jokes.base.BaseActivity;
import com.m.jokes.db.NotesTable;
import com.m.jokes.interfaces.NoteClickCallback;
import com.m.jokes.model.NotesModel;
import com.m.jokes.prefrence.AppSharedPreference;
import com.m.jokes.prefrence.PrefConstants;
import com.m.jokes.utils.SpaceItemDecoration;
import com.m.jokes.utils.Utils;
import java.util.ArrayList;
import static com.m.jokes.utils.Constants.POSITION_RIGHT_BOTTOM;

public class NotesActivity extends BaseActivity implements NoteClickCallback {

    private ArrayList<NotesModel> notesModelList;
    private NotesAdapter notesAdapter;
    private NotesTable salesDb;
    private final int ADD_NOTE_INTENT = 101;
    private final int EDIT_NOTE_INTENT = 201;
    private final int DELETE_NOTE = 202;
    RecyclerView rv_notes;
    TextView lblNoDataFound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.notes_toolbar);
        manageToolBar(toolbar,"Notes");
         rv_notes = (RecyclerView) findViewById(R.id.notes_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(NotesActivity.this);
        rv_notes.setLayoutManager(linearLayoutManager);
        rv_notes.addItemDecoration(new SpaceItemDecoration(5, false));
        lblNoDataFound=(TextView)findViewById(R.id.lbl_no_data_found);
        if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE, false, JokesApplication.getInstance()))
        {
            AdView adView = (AdView) findViewById(R.id.adView);
            adView.setVisibility(View.VISIBLE);
            adView.loadAd(Utils.getAdRefence(NotesActivity.this));
        }
        if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE,false,NotesActivity.this))
        {
            Utils.showPollifishAddRight(NotesActivity.this,POSITION_RIGHT_BOTTOM);

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
       // Utils.showPollifishAddRight(NotesActivity.this,POSITION_RIGHT_BOTTOM);
        notesModelList = new ArrayList<>();
        getNotes();
        notesAdapter = new NotesAdapter(NotesActivity.this, notesModelList,this);
        rv_notes.setAdapter(notesAdapter);
        setLblNoDatFound();
    }

    private void getNotes() {
        salesDb = new NotesTable(getApplication());
        notesModelList = salesDb.fetchNote();
        if (notesModelList!=null && notesModelList.size()==0)
            Utils.showNoteDialog(this,"You can click on + icon to add a new note");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.notes_tweet, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_note:
                Intent addNoteIntent = new Intent(NotesActivity.this, AddNoteActivity.class);
                this.startActivityForResult(addNoteIntent, ADD_NOTE_INTENT);
                break;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ADD_NOTE_INTENT:
                notesModelList.clear();
                notesModelList = salesDb.fetchNote();
                notesAdapter.updateNotesModels(notesModelList);
                notesAdapter.notifyDataSetChanged();
                break;

            case EDIT_NOTE_INTENT:
                notesModelList.clear();
                notesModelList = salesDb.fetchNote();
                notesAdapter.updateNotesModels(notesModelList);
                notesAdapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void openNote(String noteText, String noteDate,int pos) {
        Intent editNoteIntent = new Intent(NotesActivity.this, NotesDetailAtivity.class);
        editNoteIntent.putExtra("text", noteText);
        editNoteIntent.putExtra("select_note_pos", pos);
        editNoteIntent.putParcelableArrayListExtra("note_list",notesModelList);
        this.startActivity(editNoteIntent);
    }
    void setLblNoDatFound()
    {
        if(notesModelList==null||notesModelList.size()==0)
        {
            lblNoDataFound.setVisibility(View.VISIBLE);
            rv_notes.setVisibility(View.GONE);
        }else
        {
            lblNoDataFound.setVisibility(View.GONE);
            rv_notes.setVisibility(View.VISIBLE);
        }
    }


}
