package com.m.jokes.ui.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.m.jokes.R;
import com.m.jokes.application.JokesApplication;
import com.m.jokes.base.BaseActivity;
import com.m.jokes.db.NotesTable;
import com.m.jokes.model.NotesModel;
import com.m.jokes.prefrence.AppSharedPreference;
import com.m.jokes.prefrence.PrefConstants;
import com.m.jokes.utils.Utils;
import java.util.ArrayList;
import static com.m.jokes.utils.Constants.POSITION_LEFT_TOP;

public class NotesDetailAtivity extends BaseActivity {
    private ArrayList<NotesModel> notesModelList;
    private int mPosition  = 0 ;
    //    private int currentPos = 0 ;
    private TextView mTitleView ;
    private TextView mDescriptionView ;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_notes_ativity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.notes_toolbar);
        manageToolBar(toolbar,"Notes Details");
        mPosition = getIntent().getIntExtra("select_note_pos",0);
        notesModelList = getIntent().getParcelableArrayListExtra("note_list");
        initView();
        setData(mPosition);
        if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE, false, JokesApplication.getInstance()))
        {

            AdView adView = (AdView) findViewById(R.id.adView);
            adView.setVisibility(View.VISIBLE);
            adView.loadAd(Utils.getAdRefence(this));
        }
    }

    private void initView()
    {
        mTitleView = (TextView) findViewById(R.id.tv_title);
        mDescriptionView = (TextView) findViewById(R.id.tv_description);
        findViewById(R.id.btn_delete).setOnClickListener(this);
        findViewById(R.id.btn_prev).setOnClickListener(this);
        findViewById(R.id.btn_next).setOnClickListener(this);
        findViewById(R.id.btn_msg).setOnClickListener(this);

    }

    private void setData(int pos)
    {
        try
        {
            NotesModel mNoteModel = notesModelList.get(pos);
            if (mNoteModel.getNote_text().length()>0)
                mTitleView.setText((mNoteModel.getNote_text().length()>=5)? mNoteModel.getNote_text().substring(0,5): mNoteModel.getNote_text());

            mDescriptionView.setText(mNoteModel.getNote_text());
            mDescriptionView.setTag(mNoteModel.getNote_text()+","+ mNoteModel.getNote_date());
        }catch (Exception e)
        {
            mPosition=pos-1;
            setData(mPosition);
            e.printStackTrace();
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.notes_edit, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.add_note:
               String data =  mDescriptionView.getTag().toString();
               String[] selectedString = data.split(",");
               Intent addNoteIntent = new Intent(NotesDetailAtivity.this, AddNoteActivity.class);
                addNoteIntent.putExtra("text",selectedString[0]);
                addNoteIntent.putExtra("date",selectedString[1]);
                this.startActivity(addNoteIntent);
                finish();
                break;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view)
    {
        super.onClick(view);
        switch (view.getId())
        {
            case R.id.btn_delete :
                askToDelete(this);
                break;
            case R.id.btn_msg :
                shareMsg();
                break;
            case R.id.btn_prev :
               moveToPreviousNotes();
                break;
            case R.id.btn_next :
                moveToNextNotes();
                break;
        }
    }

    private void askToDelete(Context context) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        deleteNotes();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Do you want delete Notes ?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }


    private void shareMsg()
    {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, ""+mDescriptionView.getText().toString());
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Send to"));
    }
    private void moveToPreviousNotes()
    {
        if (mPosition>0)
        {
            if(mPosition>(notesModelList.size()-1))
                mPosition=(notesModelList.size()-1);
            setData(--mPosition);
        }
        else
            Utils.showNoteDialog(this,"No Notes Available");
    }

    private void moveToNextNotes()
    {
        if (mPosition<notesModelList.size()-1)
        {
            setData(++mPosition);
        }
        else
            Utils.showNoteDialog(this,"No Notes Available");
    }
    private void deleteNotes()
    {
        try {
            NotesTable mNotesTable = new NotesTable(getApplication());
            mNotesTable.deleteNote(notesModelList.get(mPosition).getNote_date());
            notesModelList.remove(mPosition);
            if(notesModelList!=null)
            {
                if (notesModelList.size() == 0)
                {
                    finish();
                } else
                {
                  finish();

                }
            }else
            {
                finish();
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        //Utils.showPollifishAddRight(NotesDetailAtivity.this,POSITION_LEFT_TOP);

    }
}
