package com.m.jokes.ui.activity;

import android.support.v4.view.MenuItemCompat;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import com.m.jokes.R;
import com.m.jokes.adapter.SubCategoryAdapter;
import com.m.jokes.base.BaseActivity;
import com.m.jokes.constant.AppConstant;
import com.m.jokes.db.JokesTable;
import com.m.jokes.model.JokesModel;
import java.util.ArrayList;

public class SearchSubCategoryActivity extends BaseActivity implements SearchView.OnQueryTextListener{
    private ArrayList<JokesModel>  CategorModelList;
    private SubCategoryAdapter CategorAdapter;
    private JokesTable salesDb;
    RecyclerView rv_Category ;
    String selected_category ;
    String type ;
    ArrayList<JokesModel> searchList ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_sub_category);
        selected_category =  getIntent().getStringExtra("category");
        type =  getIntent().getStringExtra(AppConstant.ACTIVITY_TYPE);
        Toolbar toolbar = (Toolbar) findViewById(R.id.Categor_toolbar);
        manageToolBar(toolbar,"Search SubCategory");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        rv_Category = (RecyclerView) findViewById(R.id.Categor_list);
        getCategor();
    }

    private void getCategor() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SearchSubCategoryActivity.this);
        CategorModelList = new ArrayList<>();
        salesDb = new JokesTable(getApplication());
//        CategorModelList = salesDb.fetchSubCategory(selected_category,1);
//        searchList=CategorModelList;
        CategorAdapter = new SubCategoryAdapter(SearchSubCategoryActivity.this, CategorModelList, selected_category);
        rv_Category.setLayoutManager(linearLayoutManager);
        rv_Category.setAdapter(CategorAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.notes_search, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                   // Do something when collapsed
                        CategorAdapter.setData(CategorModelList,null);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
               // Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final ArrayList<JokesModel> filteredModelList = filter(searchList, newText);
        CategorAdapter.setData(filteredModelList,null);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private ArrayList<JokesModel> filter(ArrayList<JokesModel> models, String query) {
        query = query.toLowerCase();
         ArrayList<JokesModel> filteredModelList = new ArrayList<>();
//         if (query.trim().equalsIgnoreCase(""))
//             return models;
//         else {
//             for (String text1 : models) {
//                 String text = text1.toLowerCase();
//                 if (text.contains(query)) {
//                     filteredModelList.add(text);
//                 }
//             }
             return filteredModelList;
//         }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        onAppBackPressed();
    }

    private void onAppBackPressed() {
        if (searchList!=null)
        {
            CategorAdapter.setData(CategorModelList,null);
        }else {
            finish();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
      //  Utils.showPollifishAddRight(SearchSubCategoryActivity.this,POSITION_RIGHT_BOTTOM);

    }
}
