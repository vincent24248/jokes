package com.m.jokes.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.bumptech.glide.Glide;
import com.m.jokes.DashBoardActivity;
import com.m.jokes.R;
import com.m.jokes.prefrence.AppSharedPreference;
import com.m.jokes.prefrence.PrefConstants;
import com.m.jokes.utils.Constants;
import com.m.jokes.utils.GIFView;
import com.m.jokes.utils.Globalutils;

public class SpalshActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spalsh);
        RelativeLayout linlayParent=(RelativeLayout)findViewById(R.id.linear_spash);
        //WebView webView=new WebView(this);
        //webView.loadUrl("https://static.wixstatic.com/media/c2147c_fbcf303430b44acaad6fdbcb7e5755b9~mv2.gif");
        GIFView imgGif=(GIFView)findViewById(R.id.img_view);
//         imgGif.setGIFResource(R.drawable.splash_gil);
      //  Glide.with(this).load("https://static.wixstatic.com/media/c2147c_fbcf303430b44acaad6fdbcb7e5755b9~mv2.gif").into(imgGif);
        if(!AppSharedPreference.getBoolean(PrefConstants.IS_ALARAM_INITIALISE,false,SpalshActivity.this))
        {
            Globalutils.initialiseAlarm(this);
            AppSharedPreference.putBoolean(PrefConstants.IS_ALARAM_INITIALISE,true,SpalshActivity.this);
        }

        linlayParent.setBackgroundColor(Globalutils.getRandonColor());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                success();
            }
        },2500);
    }

    void success()
    {
        Intent loginIntent = new Intent(SpalshActivity.this, DashBoardActivity.class);
        startActivity(loginIntent);
        finish();
    }
}
