package com.m.jokes.ui.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.SearchView;
import com.m.jokes.R;
import com.m.jokes.adapter.SubCategoryAdapter;
import com.m.jokes.application.JokesApplication;
import com.m.jokes.base.BaseActivity;
import com.m.jokes.constant.AppConstant;
import com.m.jokes.db.JokesTable;
import com.m.jokes.model.JokesModel;
import com.m.jokes.prefrence.AppSharedPreference;
import com.m.jokes.prefrence.PrefConstants;
import com.startapp.android.publish.ads.nativead.NativeAdDetails;
import com.startapp.android.publish.ads.nativead.NativeAdPreferences;
import com.startapp.android.publish.ads.nativead.StartAppNativeAd;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import java.util.ArrayList;
import java.util.Iterator;

public class SubCategoryActivity extends BaseActivity {
    private ArrayList<JokesModel>  CategorModelList = new ArrayList<>();
    private ArrayList<NativeAdDetails> addsModels=new ArrayList<>();
    private SubCategoryAdapter mCategorAdapter;
    RecyclerView rv_Category ;
    String selected_category ;
    String type ;
    public SearchView search;
    NativeAdDetails nativeDetail;
    int addsRequireToLoad=1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        selected_category =  getIntent().getStringExtra("category");
        type =  getIntent().getStringExtra(AppConstant.ACTIVITY_TYPE);
        Toolbar toolbar = (Toolbar) findViewById(R.id.Categor_toolbar);
        search = (SearchView) findViewById( R.id.search);
        manageToolBar(toolbar,selected_category);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        rv_Category = (RecyclerView) findViewById(R.id.Categor_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SubCategoryActivity.this);
        rv_Category.setLayoutManager(linearLayoutManager);
        mCategorAdapter = new SubCategoryAdapter(SubCategoryActivity.this, CategorModelList,selected_category);
        rv_Category.setAdapter(mCategorAdapter);
       /* if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE,false,SubCategoryActivity.this))
        {

            StartAppSDK.init(this, START_APP_ID, false); //TODO: Replace with your Application ID
            StartAppAd.disableSplash();

        }*/
        getCategor();
        configNativeStartAppAds();


    }

    private void getCategor()
    {
        CategorModelList = new ArrayList<>();
        JokesTable salesDb = new JokesTable(getApplication());
        CategorModelList = salesDb.fetchJokes(selected_category,"category");
        addsRequireToLoad=CategorModelList.size();
        mCategorAdapter.setData(CategorModelList,addsModels);
        search.setOnQueryTextListener(listener);

    }
    @Override
    protected void onResume() {
        super.onResume();
       // Utils.showPollifishAddRight(SubCategoryActivity.this,POSITION_RIGHT_BOTTOM);

    }

      /* this is the Seerach QuerttextListner.
       this method filter the list data with a matching string,
       hence provides user an easy way to find the information he needs.
     */
    SearchView.OnQueryTextListener listener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextChange(String query) {
            query = query.toLowerCase();

            final ArrayList<JokesModel> filteredList = new ArrayList<>();
              boolean isSearchedInSubcategory=false;
            for (int i = 0; i < CategorModelList.size(); i++) {
                final String text = CategorModelList.get(i).getSubCategory().toLowerCase();
                if (text.contains(query)) {
                    isSearchedInSubcategory=true;
                    filteredList.add(CategorModelList.get(i));
                }
            }
            if(!isSearchedInSubcategory)
            {
                for (int i = 0; i < CategorModelList.size(); i++)
                {
                    final String text = CategorModelList.get(i).getMessage().toLowerCase();
                    if (text.contains(query))
                    {

                        filteredList.add(CategorModelList.get(i));
                    }
                }
            }

            mCategorAdapter.setData(filteredList,addsModels);
            return true;

        }
        public boolean onQueryTextSubmit(String query) {
            return false;
        }
    };
    private void configNativeStartAppAds() {
        final StartAppNativeAd startAppNativeAd = new StartAppNativeAd(SubCategoryActivity.this);
        int addsToLoad=(addsRequireToLoad<=0)?1:addsRequireToLoad;
        Log.e("addsToLoad ",""+addsToLoad);
        NativeAdPreferences nativePrefs = new NativeAdPreferences()
                .setAdsNumber(addsToLoad)                // Load 3 Native Ads
                .setAutoBitmapDownload(true)    // Retrieve Images object
                .setPrimaryImageSize(0);        // 150x150 image
        //To load your native ad, call the loadAd() method with a NativeAdPreferences object:

        startAppNativeAd.loadAd(nativePrefs, new AdEventListener()
        {
            @Override
            public void onReceiveAd(Ad arg0)
            {
                ArrayList<NativeAdDetails> ads = startAppNativeAd.getNativeAds();    // get NativeAds list
                // Print all ads details to log
                Iterator<NativeAdDetails> iterator = ads.iterator();
                while(iterator.hasNext())
                {
                    nativeDetail=   iterator.next();
                    addsModels.add(nativeDetail);

                }
                if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE, false, JokesApplication.getInstance()))
                {
                    mCategorAdapter.setData(CategorModelList,addsModels);
                }
                Log.e("addsSizeCat ",""+addsModels.size());

            }

            @Override
            public void onFailedToReceiveAd(Ad arg0)
            {

                //  Toast.makeText(getActivity(),"onFailedToReceiveAd",Toast.LENGTH_SHORT).show();
                // Native Ad failed to receive
            }
        });
    }
}
