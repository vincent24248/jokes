package com.m.jokes.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.m.jokes.R;
import com.m.jokes.adapter.DashboardCategoryAdapter;
import com.m.jokes.application.JokesApplication;
import com.m.jokes.constant.AppConstant;
import com.m.jokes.db.JokesTable;
import com.m.jokes.model.CategoryModel;
import com.m.jokes.prefrence.AppSharedPreference;
import com.m.jokes.prefrence.PrefConstants;
import com.m.jokes.ui.activity.ActivityAddVideo;
import com.m.jokes.ui.activity.ActivityFavorite;
import com.m.jokes.ui.activity.ActivityAppOfTheDay;
import com.m.jokes.ui.activity.NotesActivity;
import com.m.jokes.ui.activity.SubCategoryActivity;
import com.m.jokes.utils.Globalutils;
import com.m.jokes.utils.Utils;
import com.startapp.android.publish.ads.nativead.NativeAdDetails;
import com.startapp.android.publish.ads.nativead.NativeAdPreferences;
import com.startapp.android.publish.ads.nativead.StartAppNativeAd;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import java.util.ArrayList;
import java.util.Iterator;


public class DashBoardFragment extends Fragment implements View.OnClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    RecyclerView rv_Category ;
    NestedScrollView nestedScrollView;
    TextView txtCategoryNameOne,txtCategoryLetterOne;
    RelativeLayout relayOne;
    TextView txtCategoryNameTwo,txtCategoryLetterTwo;
    RelativeLayout relayTwo;
   // com.startapp.android.publish.ads.banner.Banner  mAdView ;
    ArrayList<CategoryModel> categorModelList ;
    private ArrayList<CategoryModel> categoryListt;
    private ImageView addImgStartApp;
    NativeAdDetails nativeDetail;
    private RelativeLayout addsRelay;
    private TextView titleTxt,descrpTxt;
    RelativeLayout btnInstall;
    ImageView addSmallImg;
    RatingBar ratingBar;

    public DashBoardFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static DashBoardFragment newInstance(String param1, String param2) {
        DashBoardFragment fragment = new DashBoardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_dash_board, container, false);
        rv_Category = (RecyclerView) view.findViewById(R.id.Categor_list);
        nestedScrollView=(NestedScrollView) view.findViewById(R.id.scroll_c);
        addsRelay=(RelativeLayout)view.findViewById(R.id.ads_relay);
        addsRelay.setVisibility(View.GONE);
        titleTxt=(TextView)view.findViewById(R.id.text_title);
        descrpTxt=(TextView)view.findViewById(R.id.txt_description);
        btnInstall=(RelativeLayout)view.findViewById(R.id.btn_install);
        addSmallImg=(ImageView)view.findViewById(R.id.add_img_small);
        ratingBar=(RatingBar)view.findViewById(R.id.rating_bar);
        btnInstall.setOnClickListener(this);
        initView(view);
        getCategor();

        return view;
    }

    private void initView(View view) {
        txtCategoryNameOne=(TextView)view.findViewById(R.id.tv_category);
        txtCategoryLetterOne=(TextView)view.findViewById(R.id.nameTxt);
        txtCategoryLetterTwo=(TextView)view.findViewById(R.id.txt_name_two);
        relayOne=(RelativeLayout) view.findViewById(R.id.rel1);
        relayOne.setOnClickListener(this);

        txtCategoryNameTwo=(TextView)view.findViewById(R.id.tv_category1);
        relayTwo=(RelativeLayout)view.findViewById(R.id.rel2);
        relayTwo.setOnClickListener(this);

       // mAdView= (com.startapp.android.publish.ads.banner.Banner)view.findViewById(R.id.adView);
        addImgStartApp=(ImageView)view.findViewById(R.id.add_img_startapp);
       /* if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE, false, JokesApplication.getInstance()))
        {
           // mAdView.showBanner();
        }else
        {
           // mAdView.hideBanner();
        }*/
        view.findViewById(R.id.rel3).setOnClickListener(this);
        view.findViewById(R.id.rel4).setOnClickListener(this);
        view.findViewById(R.id.rel5).setOnClickListener(this);
        view.findViewById(R.id.rel6).setOnClickListener(this);

       addImgStartApp.setOnClickListener(new View.OnClickListener()
{
    @Override
    public void onClick(View view)
    {
        if(nativeDetail!=null)
        nativeDetail.sendClick(getActivity());
     }
    });

        //mAdView.loadAd(Utils.getAdRefence(context));

    }

    private void getCategor() {
        RecyclerView.LayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),3);
        categorModelList = new ArrayList<>();
        JokesTable salesDb = new JokesTable(getActivity().getApplication());
        categorModelList = salesDb.fetchCategory();
        categoryListt=salesDb.fetchCategory();
        txtCategoryNameOne.setText(categoryListt.get(0).getCategory());
        txtCategoryLetterOne.setText(categoryListt.get(0).getCategory().substring(0,1));
        relayOne.setBackgroundColor(Globalutils.getRandonColor());

        txtCategoryNameTwo.setText(categoryListt.get(1).getCategory());
        txtCategoryLetterTwo.setText(categoryListt.get(1).getCategory().substring(0,1));
        relayTwo.setBackgroundColor(Globalutils.getRandonColor());

        DashboardCategoryAdapter categorAdapter = new DashboardCategoryAdapter(getActivity(), categorModelList);
        rv_Category.setLayoutManager(gridLayoutManager);
        rv_Category.setAdapter(categorAdapter);

        rv_Category.setNestedScrollingEnabled(false);
        configNativeStartAppAds();


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rel1:
                Intent intent = new Intent(getActivity(), SubCategoryActivity.class);
                intent.putExtra("category", categoryListt.get(0).getCategory());
                intent.putExtra(AppConstant.ACTIVITY_TYPE, AppConstant.ACTIVITY_CATEGORY);
                getActivity().startActivity(intent);
                break;
            case R.id.rel2:
                Intent intentt = new Intent(getActivity(), SubCategoryActivity.class);
                intentt.putExtra("category", categoryListt.get(1).getCategory());
                intentt.putExtra(AppConstant.ACTIVITY_TYPE, AppConstant.ACTIVITY_CATEGORY);
                getActivity().startActivity(intentt);
                break;
            case R.id.rel3:
                getActivity().startActivity(new Intent(getActivity(), NotesActivity.class));
                break;
            case R.id.rel4:
                if(Utils.isNetworkConnected(getActivity())) {
                   // if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE,false,getActivity())) {
                        Intent mIntent = new Intent(getActivity(), ActivityAddVideo.class);
                        getActivity().startActivity(mIntent);
                   // }
                }else
                {
                    Toast.makeText(getActivity(),"No internet connection",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.rel5:
                getActivity().startActivity(new Intent(getActivity(), ActivityFavorite.class));
                break;
            case R.id.rel6:
                if(Utils.isNetworkConnected(getActivity())) {
                   // if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE,false,getActivity()))
                    //{
                        Intent mIntent = new Intent(getActivity(), ActivityAppOfTheDay.class);
                        getActivity().startActivity(mIntent);
                   // }
                }else
                {
                    Toast.makeText(getActivity(),"No internet connection",Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.btn_install:
                nativeDetail.sendClick(getActivity());
                break;
            default:
                break;


    }
}
    private void configNativeStartAppAds() {
        final StartAppNativeAd startAppNativeAd = new StartAppNativeAd(getActivity());
        NativeAdPreferences nativePrefs = new NativeAdPreferences()
                .setAdsNumber(1)                // Load 3 Native Ads
                .setAutoBitmapDownload(true)    // Retrieve Images object
                .setPrimaryImageSize(3);        // 150x150 image
        //To load your native ad, call the loadAd() method with a NativeAdPreferences object:

        startAppNativeAd.loadAd(nativePrefs, new AdEventListener()
        {
            @Override
            public void onReceiveAd(Ad arg0)
            {
                ArrayList<NativeAdDetails> ads = startAppNativeAd.getNativeAds();    // get NativeAds list
                // Print all ads details to log
                Iterator<NativeAdDetails> iterator = ads.iterator();
                while(iterator.hasNext())
                {
                    nativeDetail=   iterator.next();
                    nativeDetail.getCategory();
                    nativeDetail.getDescription();
                    nativeDetail.getTitle();
                    nativeDetail.getImageUrl();
                    if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE, false, JokesApplication.getInstance()))
                    {
                        titleTxt.setText(nativeDetail.getTitle());
                        descrpTxt.setText(nativeDetail.getDescription());
                        addsRelay.setVisibility(View.VISIBLE);
                        addImgStartApp.setImageBitmap(nativeDetail.getImageBitmap());
                        addSmallImg.setImageBitmap(nativeDetail.getSecondaryImageBitmap());
                        
                        ratingBar.setRating(nativeDetail.getRating());
                        if(nativeDetail.isApp())
                        {

                        btnInstall.setVisibility(View.VISIBLE);
                        }else
                        {
                            btnInstall.setVisibility(View.GONE);
                        }
                    }else
                    {
                        addsRelay.setVisibility(View.GONE);
                    }

                 //   Toast.makeText(getActivity(),"onReceiveAd",Toast.LENGTH_SHORT).show();
                    break;
                }
                Log.e("addsSizeOnMainScreen ",""+ads.size());

            }

            @Override
            public void onFailedToReceiveAd(Ad arg0)
            {
                addImgStartApp.setVisibility(View.GONE);
              //  Toast.makeText(getActivity(),"onFailedToReceiveAd",Toast.LENGTH_SHORT).show();
                // Native Ad failed to receive
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if(AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE, false, JokesApplication.getInstance()))
        {
           addsRelay.setVisibility(View.GONE);
        }else
        {
            if(nativeDetail!=null)
            addsRelay.setVisibility(View.VISIBLE);
        }
    }
}
