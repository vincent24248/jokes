package com.m.jokes.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;

/**
 * Created by SUJEET on 19-11-2017.
 *
 */

public class AppUtils {

    public static Typeface setCustomFont(Context context, String fontType) {
        String fontPath;

        fontPath = "fonts/roboto/" + fontType + ".ttf";


        Typeface typeface = Typeface.createFromAsset(context.getAssets(), fontPath);
        return typeface;
    }


    public static Typeface setRegularFont(Context context) {
        return setCustomFont(context, Constants.roboto_regular);
    }

    public static Typeface setMediumFont(Context context) {
        return setCustomFont(context, Constants.roboto_medium);
    }

    public static Typeface setThinFont(Context context) {
        return setCustomFont(context, Constants.roboto_thin);

    }

    public static Typeface setBoldFont(Context context) {
        return setCustomFont(context, Constants.roboto_bold);

    }

    public static Typeface setSemiBoldFont(Context context) {
        return setCustomFont(context, Constants.roboto_bold);

    }

    public static Typeface setLightFont(Context context) {
        return setCustomFont(context, Constants.roboto_light);

    }

    public static Typeface setItalicFont(Context context) {
        return setCustomFont(context, Constants.roboto_italic);

    }

    private static Bitmap RGB565toARGB888(Bitmap img) throws Exception {
        int numPixels = img.getWidth() * img.getHeight();
        int[] pixels = new int[numPixels];

        //Get JPEG pixels.  Each int is the color values for one pixel.
        img.getPixels(pixels, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());

        //Create a Bitmap of the appropriate format.
        Bitmap result = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);

        //Set RGB pixels.
        result.setPixels(pixels, 0, result.getWidth(), 0, 0, result.getWidth(), result.getHeight());
        return result;
    }
}
