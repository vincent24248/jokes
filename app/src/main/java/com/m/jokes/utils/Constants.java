package com.m.jokes.utils;
import java.util.Collection;

public class Constants {
    // fonts
    public static final String roboto_medium = "Roboto-Medium";
    public static final String roboto_bold = "Roboto-Bold";
    public static final String roboto_light = "Roboto-Light";
    public static final String roboto_italic = "Roboto-Italic";
    public static final String roboto_thin = "Roboto-Thin";
    public static final String roboto_regular = "Roboto-Regular";
    public static final String PREF_NAME = "mjokesPref";
    public static final String proxima_regular = "ProximaNova-Reg";
    public static final String proxima_bold = "ProximaNova-Bold";
    public static final String proxima_light = "ProximaNova-Light";
    public static final String proxima_thin = "ProximaNova-Thin";
    public static  String IS_COMING_FROM="IS_COMING_FROM";
    public static int FROM_NOTIFICATION=1;
    public static int FROM_FAVOURITE=2;
    public static int FROM_SUBCATEGORY=3;


    public static int POSITION_RIGHT_TOP=0;
    public static int POSITION_LEFT_TOP=1;
    public static int POSITION_RIGHT_BOTTOM=2;
    public static int POSITION_LEFT_BOTTOM=3;



public static int SPEECH_RECOGNITION_CODE=133;

    //UNIQUE IDS TO USE
    public static String FACEBOOK_LINK="https://www.facebook.com/Informative-Apps-198393130733500/";
    public static String GOOGLE_PLUS_LINK="https://plus.google.com/115407345868841669438?hl=en";
    public static String UTUBE_LINK="https://www.youtube.com/channel/UC3IyPe8_hDHj_klEdC9dBig";
    public static String GOOGLE_PLAY_STORE_LINK="https://play.google.com/store/apps/details?id=com.jokes.informative.app";
    public static String SHOP_NOW_URL="http://www.tingomart.com/";
    public static String OUR_MORE_APPS="https://play.google.com/store/apps/dev?id=8260718226722335249";//http://www.tingomart.com/";

    //ADD MOB APP ID
    public static String ADD_MOB_APP_ID="ca-app-pub-2446311341951677~1123187328";
    //Addmob banner id
    public static String ADD_MOB_BANNER_ADD_UNIT_ID="ca-app-pub-2446311341951677/8421370649";//"ca-app-pub-3940256099942544/6300978111";//
    //ADDmob interetial ID
    public static String  ADDMOB_INTERSTITIALAD_ID="ca-app-pub-2446311341951677/6120965775";//"ca-app-pub-3940256099942544/1033173712";//

    //App Next ->App of the DAY
    public static String  AppNext="e5b5c75a-d659-4d21-841b-74e09ba48c7f";//2623d957-778b-4918-b471-7d31975e757d";//""3a71faa1-f7d2-4285-830a-4a4fd8d0e340";2623d957-778b-4918-b471-7d31975e757d

     // startApp -> rectangle Add
    public static  String   START_APP_ID="210814863";


    public static  String  POLLFISH  ="e6ef458b-4943-440b-8ce5-b6c8b1c9927e";

    public static String IN_APP_BILLING_LICENCE_KEY="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApkW6Hp7JB0yEOQrgmTpJ6C/Vd0fH+JvT8EwA0TIIgFgnIDfne16dW79n5HgxW/4lhdX3H+v5AUDhrYqtI6IPZ5kzwtchBYWb6IsXvgpCndrEMGYvSUibEKjVoCVd7k1TYXxJBtQcZygi+e1zIU0NYMT24WphFtZF2111HnueVc/qnjKANrYsAcDQufTDO9SHpchp3A0VDOs2Iv4PB+71yrFFWsC8XNXwoYZrpY/z0HnJV6TP0SQRjQ/rW1XpOdK7gZNUUBRvmY9Pvi8fN1y7z+N2KnyISblNhhdIqnQBl+HvvfNwQ+xsw6WZ74ytrwTFdYKzHUVkAO/tpGRMgJgMNwIDAQAB";

    public static String INAPP_BILLING_RODUCT_KEY="funny_jokes_ads_free";
    public static String TEST_INAPP_BILLING_PRODUCT_ID="android.test.purchased";


    public static int DEFAULT_TEXT_SIZE=22;
    public static int DEFAULT_NOTIFICATION_COUNT=5;


    /*public static String INMOBI_ACCOUNT_ID="433643682d12463a89ed7966a186710c:" ;
    public static String INMOBI_APP_ID="1511535688807";
    public  static String INMOBI_VIDEO_ADD_PLACEMENT_ID="1510813111038";
    public static String INMOBI_LISTING_NATIVE_AD_PLACEMENT_ID= "1512746856895";*/

}