package com.m.jokes.utils;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.m.jokes.R;
import com.m.jokes.interfaces.OnDialogButtonClicked;
import com.m.jokes.prefrence.AppSharedPreference;
import com.m.jokes.prefrence.PrefConstants;
import com.m.jokes.receiver.AlarmReceiver;
import com.m.jokes.services.JokesJobService;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
import java.util.Calendar;
import java.util.Random;

import static android.os.Build.VERSION_CODES.LOLLIPOP_MR1;
import static com.m.jokes.utils.Constants.DEFAULT_NOTIFICATION_COUNT;
import static com.m.jokes.utils.Constants.FACEBOOK_LINK;
import static com.m.jokes.utils.Constants.GOOGLE_PLAY_STORE_LINK;
import static com.m.jokes.utils.Constants.GOOGLE_PLUS_LINK;
import static com.m.jokes.utils.Constants.OUR_MORE_APPS;
import static com.m.jokes.utils.Constants.SHOP_NOW_URL;
import static com.m.jokes.utils.Constants.UTUBE_LINK;

/**
 * Created by saten on 11/17/17.
 *
 */

public class Globalutils {


    public static int getRandonColor()
    {
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

    }
    public static void openGoogleplayStore(Context mContext){
        final String appPackageName = mContext.getPackageName(); // getPackageName() from Context or Activity object
        try {
            mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(GOOGLE_PLAY_STORE_LINK)));
        } catch (android.content.ActivityNotFoundException anfe) {
            mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(GOOGLE_PLAY_STORE_LINK)));
        }
    }

    public static void openFb(Context mContext) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/426253597411506"));
            mContext.startActivity(intent);
        } catch (Exception e) {
            mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/appetizerandroid")));
        }
    }

    public static void shareApp(Context mContext) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = mContext.getResources().getString(R.string.share_body);
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, R.string.share_subject);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        Intent intent = Intent.createChooser(sharingIntent, "Share via");
        mContext.startActivity(intent);
    }

    public static void resetInitialisedAlarm(Context mContext,int testStatus)
    {
        Intent alarmIntent = new Intent(mContext, AlarmReceiver.class);
        alarmIntent.setData(Uri.parse("custom://" + "123"));
        alarmIntent.setAction(String.valueOf("123"));
        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(mContext.ALARM_SERVICE);
        PendingIntent displayIntent = PendingIntent.getBroadcast(mContext, 0, alarmIntent, 0);
        alarmManager.cancel(displayIntent);
    }

    //Rate Us Dialog Functionality
    public static   void notificationSettingDailog(final Context mContext, final OnDialogButtonClicked onDialogButtonClicked) {
        final Dialog dialogRating = new Dialog(mContext);
        dialogRating.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogRating.setContentView(R.layout.dialog_rate_us);
        final DiscreteSeekBar ratingBar = (DiscreteSeekBar) dialogRating.findViewById(R.id.rating_bar);
        final TextView txtNotfiyMeXTimes=(TextView)dialogRating.findViewById(R.id.txt_notifiy_me_x_times);
        final RelativeLayout btnSubmit = (RelativeLayout) dialogRating.findViewById(R.id.btn_submit);
       final TextView lblCount=(TextView)dialogRating.findViewById(R.id.rating_value);
        if(AppSharedPreference.getInt(PrefConstants.NOTIFICATION_COUNT,DEFAULT_NOTIFICATION_COUNT,mContext)==1)
        {
            txtNotfiyMeXTimes.setText("Notify me "+AppSharedPreference.getInt(PrefConstants.NOTIFICATION_COUNT,DEFAULT_NOTIFICATION_COUNT,mContext)+" time per day");
        }else
        {
            txtNotfiyMeXTimes.setText("Notify me "+AppSharedPreference.getInt(PrefConstants.NOTIFICATION_COUNT,DEFAULT_NOTIFICATION_COUNT,mContext)+" times per day");
        }
        ratingBar.setProgress(AppSharedPreference.getInt(PrefConstants.NOTIFICATION_COUNT,DEFAULT_NOTIFICATION_COUNT,mContext));
        lblCount.setText(""+AppSharedPreference.getInt(PrefConstants.NOTIFICATION_COUNT,DEFAULT_NOTIFICATION_COUNT,mContext));
        ratingBar.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
                if(value==1)
                {
                    txtNotfiyMeXTimes.setText("Notify me "+value+" time per day");
                }else
                {
                    txtNotfiyMeXTimes.setText("Notify me "+value+" times per day");
                }
                lblCount.setText(""+value);

            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {

            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                AppSharedPreference.putInt(PrefConstants.NOTIFICATION_COUNT,5/*Integer.parseInt(lblCount.getText().toString())*/,mContext);
                initialiseAlarm(mContext);
                dialogRating.dismiss();

            }
        });


        dialogRating.show();
    }

    public static void initialiseAlarm(Context mContext)
    {


        int percount=AppSharedPreference.getInt(PrefConstants.NOTIFICATION_COUNT,DEFAULT_NOTIFICATION_COUNT,mContext);
        float intervalInHours=24.f/percount;
        float interValBwNotificationInMinutes=intervalInHours*60;
        long milisecondsToWakeUpAarm= 1000 * 60 * (int)interValBwNotificationInMinutes;
        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(mContext.ALARM_SERVICE);
            //create new Alarm
            Calendar calendaToday = Calendar.getInstance();
            int hourOfTheDay=calendaToday.get(Calendar.HOUR_OF_DAY);
            int minutesOfTheDay=calendaToday.get(Calendar.MINUTE);
           if(minutesOfTheDay>55)
            {
             hourOfTheDay= hourOfTheDay+1;
            }
            //calendarFuture.setTimeInMillis(System.currentTimeMillis());
            //calendarFuture.set(Calendar.DATE,1);
             Calendar calendarFuture = Calendar.getInstance();
             calendarFuture.add(Calendar.DATE, (hourOfTheDay>=8)?1:0);
             calendarFuture.set(Calendar.HOUR_OF_DAY,8);
             calendarFuture.set(Calendar.MINUTE, 0);
             calendarFuture.set(Calendar.SECOND, 0);

            Intent intent = new Intent(mContext, AlarmReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
           // alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendarFuture.getTimeInMillis(),milisecondsToWakeUpAarm, pendingIntent);
            // alarmManager.set(AlarmManager.RTC_WAKEUP, calendarFuture.getTimeInMillis(), pendingIntent);
           // alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendarFuture.getTimeInMillis(), milisecondsToWakeUpAarm, pendingIntent);
             Log.e("cal Future ",""+calendarFuture.getTimeInMillis());
            Log.e("cal interval ",""+milisecondsToWakeUpAarm);


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            scheduleJob(mContext);
        }
         else  if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT&&android.os.Build.VERSION.SDK_INT<=LOLLIPOP_MR1)
            {
                // only for kitkat and newer versions
               // alarmManager.setExact(AlarmManager.RTC_WAKEUP, futureTimeMiliseconds, pendingIntent);
               /* if(android.os.Build.VERSION.SDK_INT<= Build.VERSION_CODES.LOLLIPOP_MR1)
                {*/
                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendarFuture.getTimeInMillis(), milisecondsToWakeUpAarm, pendingIntent);
               /* }else
                {
                    AlarmManager.AlarmClockInfo alarmClockInfo=new AlarmManager.AlarmClockInfo(calendarFuture.getTimeInMillis(),pendingIntent);
                    alarmManager.setAlarmClock(alarmClockInfo,  pendingIntent);
                }*/
            }
            else
            {
                //alarmManager.set(AlarmManager.RTC_WAKEUP, calendarFuture.getTimeInMillis(), pendingIntent);
                alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendarFuture.getTimeInMillis(), milisecondsToWakeUpAarm, pendingIntent);
            }
    }


    public static void scheduleJob(Context context)
    {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //cancel all jobs
            JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
            jobScheduler.cancelAll();


            int percount = AppSharedPreference.getInt(PrefConstants.NOTIFICATION_COUNT, DEFAULT_NOTIFICATION_COUNT, context);
            float intervalInHours = 24.f / percount;
            float interValBwNotificationInMinutes = intervalInHours * 60;
            long milisecondsToWakeUpAarm = 1000 * 60 * (int) interValBwNotificationInMinutes;


            ComponentName serviceComponent = new ComponentName(context, JokesJobService.class);
            JobInfo.Builder builder = new JobInfo.Builder(0, serviceComponent);
            builder.setMinimumLatency(milisecondsToWakeUpAarm); // wait at least  1 * 1000
            builder.setOverrideDeadline(3 * 1000); // maximum delay
            //builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED); // require unmetered network
            //builder.setRequiresDeviceIdle(true); // device should be idle
            //builder.setRequiresCharging(false); // we don't care if the device is charging or not
            JobScheduler jobSchedulerr = context.getSystemService(JobScheduler.class);
            jobSchedulerr.schedule(builder.build());

        }
    }





    public static void showNow(Context mContext)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(SHOP_NOW_URL));
        mContext.startActivity(browserIntent);
    }

    public static void ourMoreApps(Context mContext)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(OUR_MORE_APPS));
        mContext.startActivity(browserIntent);
    }




    public static void openFacebookLink(Context mContext)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(FACEBOOK_LINK));
        mContext.startActivity(browserIntent);
    }
    public static void openUtubeLink(Context mContext)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(UTUBE_LINK));
        mContext.startActivity(browserIntent);
    }
    public static void openGoogleLink(Context mContext)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(GOOGLE_PLUS_LINK));
        mContext.startActivity(browserIntent);
    }
}
