package com.m.jokes.utils;

/**
 * Created by saten on 9/30/17.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.m.jokes.R;

import java.lang.Thread.UncaughtExceptionHandler;

public class RoundedImageView extends ImageView implements UncaughtExceptionHandler{
    public RoundedImageView(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
    }
    public RoundedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public RoundedImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    @Override
    protected void onDraw(Canvas canvas) {
        Drawable drawable = getDrawable();
        if (drawable == null) {
            return;
        }
        if (getWidth() == 0 || getHeight() == 0) {
            return;
        }
        try {
            Bitmap b =  ((BitmapDrawable)drawable).getBitmap() ;
            Bitmap bitmap = b.copy(Config.ARGB_8888, true);
            int w = getWidth();
            Bitmap roundBitmap =  getCroppedBitmap(bitmap, w);
            canvas.drawBitmap(roundBitmap, 0,0, null);
        }
        catch (Exception e) {
        }
    }
    public static Bitmap getCroppedBitmap(Bitmap bmp, int radius) {
        Bitmap sbmp;
        if(bmp.getWidth() != radius || bmp.getHeight() != radius)
            sbmp = Bitmap.createScaledBitmap(bmp, radius, radius, false);
        else
            sbmp = bmp;
        Bitmap output = Bitmap.createBitmap(sbmp.getWidth(),sbmp.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, sbmp.getWidth(), sbmp.getHeight());
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);
        canvas.drawARGB(0,0, 0, 0);
        paint.setColor(Color.parseColor("#000000"));
        canvas.drawCircle(sbmp.getWidth() / 2, sbmp.getHeight() / 2,sbmp.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(sbmp, rect, rect, paint);
        return output;
    }
    @Override
    public void uncaughtException(Thread arg0, Throwable arg1) {
    }
    public Bitmap getCircleBitmap(Bitmap paramBitmap, int paramInt){
        Bitmap localBitmap = Bitmap.createBitmap(paramBitmap.getWidth(), paramBitmap.getHeight(), Config.ARGB_8888);
        Canvas localCanvas = new Canvas(localBitmap);
        Paint localPaint = new Paint();
        Rect localRect = new Rect(0, 0, paramBitmap.getWidth(), paramBitmap.getHeight());
        RectF localRectF = new RectF(localRect);
        localPaint.setAntiAlias(true);
        localPaint.setDither(true);
        localPaint.setFilterBitmap(true);
        localCanvas.drawARGB(0,0, 0,0);
        localPaint.setColor(getResources().getColor(R.color.white));
        localCanvas.drawOval(localRectF, localPaint);
        localPaint.setColor(getResources().getColor(R.color.white));
        localPaint.setStyle(Paint.Style.STROKE);
        localPaint.setStrokeWidth(4.0F);
        localPaint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        localCanvas.drawBitmap(paramBitmap, localRect, localRect, localPaint);
        return localBitmap;
    }
}