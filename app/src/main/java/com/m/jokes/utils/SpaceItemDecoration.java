package com.m.jokes.utils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

public class SpaceItemDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SpaceItemDecoration(int space, boolean includeHeaders) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
//        if (view instanceof TextView)
//            outRect.left = 0;
//        else {
//            outRect.left = space;
//        }
        outRect.left = space;

        outRect.right = space;
        outRect.bottom = space;


        // Add top margin only for the first item to avoid double space between items
        if (parent.getChildLayoutPosition(view) == 0) {
            outRect.top = space;
        } else {
            outRect.left = space;
            outRect.top = space;

//            outRect.top = 0;
        }
        if (view instanceof LinearLayout)
            outRect.left = 0;
        else {
            outRect.left = space;
        }
    }
}