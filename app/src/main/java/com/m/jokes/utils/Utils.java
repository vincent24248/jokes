package com.m.jokes.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.support.v7.app.AlertDialog;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.widget.Button;
import android.widget.LinearLayout;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.m.jokes.application.JokesApplication;
import com.m.jokes.prefrence.AppSharedPreference;
import com.m.jokes.prefrence.PrefConstants;
import com.pollfish.constants.Position;
import com.pollfish.main.PollFish;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import static android.text.format.DateUtils.getRelativeTimeSpanString;
import static com.m.jokes.utils.Constants.ADDMOB_INTERSTITIALAD_ID;
import static com.m.jokes.utils.Constants.ADD_MOB_APP_ID;
import static com.m.jokes.utils.Constants.ADD_MOB_BANNER_ADD_UNIT_ID;
import static com.m.jokes.utils.Constants.POLLFISH;
import static com.m.jokes.utils.Constants.POSITION_LEFT_BOTTOM;
import static com.m.jokes.utils.Constants.POSITION_LEFT_TOP;
import static com.m.jokes.utils.Constants.POSITION_RIGHT_BOTTOM;
import static com.m.jokes.utils.Constants.POSITION_RIGHT_TOP;

public class Utils {

    static InterstitialAd mInterstitialAd;
    public static Typeface setCustomFont(Context context, String fontType) {
        String fontPath;

        fontPath = "fonts/roboto/" + fontType + ".ttf";


        Typeface typeface = Typeface.createFromAsset(context.getAssets(), fontPath);
        return typeface;
    }


    public static Typeface setRegularFont(Context context) {
        return setCustomFont(context, Constants.roboto_regular);
    }

    public static Typeface setMediumFont(Context context) {
        return setCustomFont(context, Constants.roboto_medium);
    }

    public static Typeface setThinFont(Context context) {
        return setCustomFont(context, Constants.roboto_thin);

    }

    public static Typeface setBoldFont(Context context) {
        return setCustomFont(context, Constants.roboto_bold);

    }

    public static Typeface setSemiBoldFont(Context context) {
        return setCustomFont(context, Constants.roboto_bold);

    }

    public static Typeface setLightFont(Context context) {
        return setCustomFont(context, Constants.roboto_light);

    }



    public static int checkProfilePicSize(Context context) {
        int density = context.getResources().getDisplayMetrics().densityDpi;
        int profilePicSize = 140;
        switch (density) {
            case DisplayMetrics.DENSITY_LOW:
                System.out.print("**DENSITY_LOW");
                profilePicSize = 70;

                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                System.out.print("**DENSITY_MEDIUM");
                profilePicSize = 100;

                break;
            case DisplayMetrics.DENSITY_HIGH:
                System.out.print("**DENSITY_HIGH");
                profilePicSize = 140;

                break;
            case DisplayMetrics.DENSITY_XHIGH:
                System.out.print("**DENSITY_XHIGH");
                profilePicSize = 185;
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                System.out.print("**DENSITY_XXHIGH");
                profilePicSize = 265;

                break;
            case DisplayMetrics.DENSITY_XXXHIGH:
                System.out.print("**DENSITY_XXXHIGH");
                profilePicSize = 300;

                break;
            default:
                break;
        }
        return profilePicSize;
    }

    public static Typeface setProximaCustomFont(Context context, String fontType) {
        String fontPath;

        fontPath = "fonts/proxima-nova/" + fontType + ".otf";


        Typeface typeface = Typeface.createFromAsset(context.getAssets(), fontPath);
        return typeface;
    }


    public static Typeface setProximaRegularFont(Context context) {
        return setProximaCustomFont(context, Constants.proxima_regular);
    }

    public static Typeface setProximaThinFont(Context context) {
        return setProximaCustomFont(context, Constants.proxima_thin);

    }

    public static Typeface setProximaBoldFont(Context context) {
        return setProximaCustomFont(context, Constants.proxima_bold);

    }


    public static Typeface setProximaLightFont(Context context) {
        return setProximaCustomFont(context, Constants.proxima_light);

    }

    public static void showNoteDialog(Context ctx , String title) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder.setMessage(title);
        alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });

        final AlertDialog dialog = alertDialogBuilder.create();
        dialog.setCancelable(true);
        dialog.show();
        final Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
        positiveButtonLL.gravity = Gravity.CENTER;
        positiveButton.setLayoutParams(positiveButtonLL);
    }
    public static String timestring(String lastUpdated) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date;
        String timePassedString = null;
        try {
            date = df.parse(lastUpdated);
            /*date.setHours(date.getHours() - 5);
            date.setMinutes(date.getMinutes() - 30);
            date.setSeconds(date.getSeconds() - 36);*/
            long epoch = date.getTime();

            timePassedString = (String) getRelativeTimeSpanString(epoch, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
            System.out.print(timePassedString);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return timePassedString;
    }

    public String timeAt(String lastUpdated) {

        DateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timePassedString = null;
        try {
            Date d = f.parse(lastUpdated);
            d.setHours(d.getHours() - 5);
            d.setMinutes(d.getMinutes() - 30);
            DateFormat time = new SimpleDateFormat("hh:mm a");
            timePassedString = time.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timePassedString;
    }

     public static  AdRequest getAdRefence(Activity activity)
     {
         MobileAds.initialize(activity, ADD_MOB_BANNER_ADD_UNIT_ID);
         AdRequest adRequest = new AdRequest.Builder().build();
         return adRequest ;
     }
    public static void showPollifishAddRight(Activity activity,int position)
    {
        if(!AppSharedPreference.getBoolean(PrefConstants.IS_ADDS_FREE, false, JokesApplication.getInstance()))
        {
            PollFish.ParamsBuilder paramsBuilder = new PollFish.ParamsBuilder(POLLFISH);
            if (position == POSITION_RIGHT_TOP)
            {
                paramsBuilder.indicatorPosition(Position.MIDDLE_RIGHT).build();
            } else if (position == POSITION_LEFT_TOP)
            {
                paramsBuilder.indicatorPosition(Position.MIDDLE_RIGHT).build();
            } else if (position == POSITION_RIGHT_BOTTOM)
            {
                paramsBuilder.indicatorPosition(Position.MIDDLE_RIGHT).build();
            } else if (position == POSITION_LEFT_BOTTOM)
            {
                paramsBuilder.indicatorPosition(Position.MIDDLE_RIGHT).build();
            }
            paramsBuilder.releaseMode(true);
            PollFish.initWith(activity, paramsBuilder);

        }
    }
    public static InterstitialAd intrestialAddbject(Context mActivity)
    {
       // if(mInterstitialAd==null) {
            MobileAds.initialize(mActivity, ADD_MOB_APP_ID);
             mInterstitialAd = new InterstitialAd(mActivity);
            mInterstitialAd.setAdUnitId(ADDMOB_INTERSTITIALAD_ID);
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
       // }
        return mInterstitialAd;
    }
    public static boolean isNetworkConnected(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

}